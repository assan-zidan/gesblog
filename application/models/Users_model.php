<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Users_model extends CI_Model
{
	function __construct()
	{
	}

	private $id;
	private $nom;
	private $email;
	private $photo_profil;
	private $niveau;

	protected $table = 'users';



    	// fonction qui recupère juste le nom  et le photo_profil de l'admin

			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
		}


	public function compte($where = array()){
		return (int) $this->db->where($where)->count_all_results($this->table);
	}

	

	// fonction qui recupere les information sur les utilsateur
	public function findUsersInfos($id){
				$data =$this->db->select('nom,email,photo_profil')
						->from($this->table)
						->where(array('id'=>$id))
						// ->limit(1)
						->get()
						->result();

				$donnees['data']='non';			
				foreach ($data as $row){
			       	$donnees['nom']=$row->nom;
			       	$donnees['email']=$row->email;
			       	$donnees['photo_profil']=$row->photo_profil;
			       	$donnees['data']='ok';
				}

				return $donnees;
			}


 		// fonction qui recupère juste le nom  et le photo_profil du moderateur

			public function findModInfos($id){
				$data =$this->db->select('nom,photo_profil')
						->from($this->table)
						->where(array('id'=>$id))
						->limit(1)
						->get()
						->result();

				$donnees['data']='non';			
				foreach ($data as $row){
			       	$donnees['nom']=$row->nom;
			       	$donnees['photo_profil']=$row->photo_profil;
			       	$donnees['data']='ok';
				}

				return $donnees;
			}

		// fonction qui recupère juste le nom  et le photo_profil du rédacteur

			public function findRedInfos($id){
				$data =$this->db->select('nom,photo_profil')
						->from($this->table)
						->where(array('id'=>$id))
						->limit(1)
						->get()
						->result();

				$donnees['data']='non';			
				foreach ($data as $row){
			       	$donnees['nom']=$row->nom;
			       	$donnees['photo_profil']=$row->photo_profil;
			       	$donnees['data']='ok';
				}

				return $donnees;
			}


			// fonction qui recupère juste le nom  et le photo_profil de l'abonné

			public function findAboInfos($id){
				$data =$this->db->select('nom,photo_profil')
						->from($this->table)
						->where(array('id'=>$id))
						->limit(1)
						->get()
						->result();

				$donnees['data']='non';			
				foreach ($data as $row){
			       	$donnees['nom']=$row->nom;
			       	$donnees['photo_profil']=$row->photo_profil;
			       	$donnees['data']='ok';
				}

				return $donnees;
			}



	//fonction qui insere les donnees venant d'un formulaire 
		public function addUser(){

		    $this->db->set('id', $this->id)
		    	->set('nom', $this->nom)
		    	->set('photo_profil', $this->photo_profil)
		    	->set('niveau', $this->niveau)
		    	->set('email', $this->email)
				->insert($this->table);
			$data =$this->db->select('id')
							->from($this->table)
							->where('email', $this->email)
							->limit(1)
							->get()
							->result();
				$donnees=array();
				$donnees['data']="non";
				foreach ($data as $row){
			       	$donnees['id']=$row->id;
			       	$donnees['data']="ok";
				}

			return $donnees['id'];
	
		}


		public function modUser($id){
			$data =$this->db->set('nom', $this->nom)
							->set('email', $this->email)
							->where('id', $id)
							->update($this->table);
		}


		// fonction qui supprime un utilisateur

		public function suppUsers($id){
			$data =$this->db->set('nom', $this->nom)
							->set('email', $this->email)
							->set('photo_profil', $this->photo_profil)
							->set('niveau', $this->niveau)
							->where('id', $id)
							->update($this->table);
		}

		

		// fonction qui charge tous les Users pour faire le filtrage de donnee
			
			public function findAllUsersBd(){
				$data = $this->db->select('id,nom,photo_profil,email,niveau')
								->from($this->table)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['nom']=$row->nom;
			       	$donnees[$i]['email']=$row->email;
			       	$donnees[$i]['photo_profil']=$row->photo_profil;
			       	$donnees[$i]['niveau']=$row->niveau;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}

			// fonction qui charge tous les redacteurs
			
			public function findAllUsers_redacBd(){
				$data = $this->db->select('id,nom,photo_profil,email,niveau')
								->from($this->table)
								->order_by('id','desc')
								->where('niveau', 3)
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['nom']=$row->nom;
			       	$donnees[$i]['email']=$row->email;
			       	$donnees[$i]['photo_profil']=$row->photo_profil;
			       	$donnees[$i]['niveau']=$row->niveau;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}

		// fonction qui charge tous les users désactivés ou supprimés
			
			public function findAllUsersSupBd(){
				$data = $this->db->select('id,nom,photo_profil,email,niveau')
								->from($this->table)
								->order_by('id','desc')
								->where('niveau', 5)
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['nom']=$row->nom;
			       	$donnees[$i]['email']=$row->email;
			       	$donnees[$i]['photo_profil']=$row->photo_profil;
			       	$donnees[$i]['niveau']=$row->niveau;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}



		// fonction qui charge les Abonnés et  les rédacteurs pour faire le filtrage de donnee
			
			public function findAboInfosBd(){
				$data = $this->db->select('id,nom,photo_profil,email,niveau')
								->from($this->table)
								->where('niveau',4)
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['nom']=$row->nom;
			       	$donnees[$i]['email']=$row->email;
			       	$donnees[$i]['photo_profil']=$row->photo_profil;
			       	$donnees[$i]['niveau']=$row->niveau;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}
			public function findRedInfosBd(){
				$data = $this->db->select('id,nom,photo_profil,email,niveau')
								->from($this->table)
								->where('niveau',3)
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['nom']=$row->nom;
			       	$donnees[$i]['email']=$row->email;
			       	$donnees[$i]['photo_profil']=$row->photo_profil;
			       	$donnees[$i]['niveau']=$row->niveau;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}


		// fonction qui recupere le niveau des Users pour faire le filtrage de donnee
			

			// public function findEndUsersBd(){
			// 	$data = $this->db->select('id,nom,photo_profil,email,niveau')
			// 					->from($this->table)
			// 					->order_by('id','desc')
			// 					->limit(1)
			// 					->get()
			// 					->result();

			// 	$i=0;
			// 	$donnees['data'] = 'non';	

				// foreach ($data as $row){
			 //       	$donnees[$i]['id']=$row->id;
			 //       	$donnees[$i]['nom']=$row->nom;
			 //       	$donnees[$i]['email']=$row->email;
			 //       	$donnees[$i]['photo_profil']=$row->photo_profil;
			 //       	$donnees[$i]['niveau']=$row->niveau;
			 //       	$i++;
			 //       	$donnees['data']='ok';
				// }
				
			// 	$donnees['total']=$i;
			// 	return $donnees;	
			// }


			public function findniveaUsersBd(){
				$data = $this->db->select('niveau')
								->from($this->table)
								// ->where('id', $this->id)
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['niveau']=$row->niveau;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}


		

	// definition des getteurs et des setteurs


	public function setId($id)
	{
		$this->id = $id;
	}

	public function setNom($nom)
	{
		$this->nom = $nom;
	}

	public function setEmail($email)
	{
		$this->email = $email;
	}

	public function setPhoto_profil($photo_profil)
	{
		$this->photo_profil = $photo_profil;
	}

	public function setNiveau($niveau)
	{
		$this->niveau = $niveau;
	}


	// getteurs

	public function getId()
	{
		return $this->id;
	}

	public function getNom()
	{
		return $this->nom;
	}

	public function getEmail()
	{
		return $this->email;
	}

	public function getPhoto_profil()
	{
		return $this->photo_profil;
	}

	public function getNiveau()
	{
		return $this->niveau;
	}
}
