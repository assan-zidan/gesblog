<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Abonne_model extends CI_Model{
	function __construct()
		{
		
		}

		private $id;
		private $id_users;
		private $email;
		private $pwd;
		
		
		protected $table = 'abonne';

		public function hydrate(array $donnees){
			foreach ($donnees as $key => $value){
				$method = 'set'.ucfirst($key);
				if (method_exists($this, $method)){
					$this->$method($value);
				}
			}
		}

		public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
		}



		public function addAbo(){

		    $this->db->set('id', $this->id)
		    		 ->set('id_users', $this->id_users)
		    	     ->set('email', $this->email)
		    	     ->set('pwd', $this->pwd)
				     ->insert($this->table);
	
		}

// fonction qui charge tous les Abonnés pour faire le filtrage de donnees
			
			public function findAllAboBd(){
				$data = $this->db->select('id,id_users,email,pwd')
								->from($this->table)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['id_users']=$row->id_users;
			       	$donnees[$i]['email']=$row->email;
			       	$donnees[$i]['pwd']=$row->pwd;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}


		// fonction qui recupère juste l'email d'un abonné

			public function findAboemail($id){
				$data =$this->db->select('email')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();

								
				foreach ($data as $row){
			       	$donnees['email']=$row->email;
				}

				return $donnees['email'];
			}



		// fonction qui recupère juste l'id_users d'un abonné

			public function findAboId_users($id){
				$data =$this->db->select('id_users')
								->from($this->table)
								->where('id', $id)
								// ->limit(1)
								->get()
								->result();

				$i=0;
				// $donnees['data'] = 'non';

				foreach ($data as $row){
			       	$donnees[$i]['id_users']=$row->id_users;
			       	$i++;
			       	$donnees['data'] = 'ok';

				}
				// $donnees['total']=$i;
				return $donnees;
			}

		// fonction qui recupère juste le password d'un abonné

			public function findAbopassword($id){
				$data =$this->db->select('pwd')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();

					$donnees['data']='non';				
				foreach ($data as $row){
			       	$donnees['pwd']=$row->pwd;
			       	$donnees['data']='ok';
				}

				return $donnees['pwd'];
			}
			


		// fonction qui reccupère juste le password et email d'un abonné

			public function findAboInfos($email,$pwd){
				$data =$this->db->select('pwd,email')
						->from($this->table)
						->where(array('pwd'=>$pwd,'email'=>$email))
						->limit(1)
						->get()
						->result();

				$donnees['data']='non';			
				foreach ($data as $row){
			       	$donnees['email']=$row->email;
			       	$donnees['pwd']=$row->pwd;
			       	$donnees['data']='ok';
				}

				return $donnees;
			}


		// fonction qui recupere qui le dernier abonne de la table
			public function findEndAbo(){
				$data = $this->db->select('id')
								->from($this->table)
								->order_by('id','desc')
								->limit(1)
								->get()
								->result();

				foreach ($data as $row){
			       	$donnees['id']=$row->id;
			       	
				}
				
				// $donnees['total']=$i;
				return $donnees['id'];	
			}

	//fonction qui insere les donnees venant d'un formulaire 
		public function addAbonne(){

		    $this->db->set('id', $this->id)
		    	->set('id_users', $this->id_users)
		    	->set('email', $this->email)
		    	->set('pwd', $this->pwd)
				->insert($this->table);		
		}

	// fonction qui modifie un admin

		public function modAbonne(){
			$data =$this->db->set('email', $this->email )
							->set('pwd', $this->pwd )
							->where('id_users', $this->id_users)
							->update($this->table);
		}

	// fonction qui permet de supprimer un admin

			public function suppAbo($id){
				$this->db->where('id_users', $id)
						 ->delete($this->table);
			}




	// definition des getteurs et des setteurs


		// setteurs

		public function setId($id){
			$this->id = $id;
		}

		public function setId_users($id_users){
			$this->id_users =$id_users;
		}

		public function setEmail($email){
			$this->email =$email;
		}

		public function setPwd($pwd){
			$this->pwd =$pwd;
		}


		// getteurs

		public function getId(){
			return $this->id;
		}

		public function getId_users(){
			return $this->id_users;
		}

		public function getEmail(){
			return $this->email;
		}

		public function getPwd(){
			return $this->pwd;
		}


}		