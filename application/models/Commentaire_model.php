<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Commentaire_model extends CI_Model{
	function __construct()
		{
		
		}

		private $id;
		private $id_abonne;
		private $id_article;
		private $contenu;
		private $date_time;
		
		protected $table = 'commentaire';



		public function hydrate(array $donnees){
			foreach ($donnees as $key => $value){
				$method = 'set'.ucfirst($key);
				if (method_exists($this, $method)){
					$this->$method($value);
				}
			}
		}


		public function compte($where = array()){
			return (int) $this->db->where($where)->count_all_results($this->table);
		}

	//gestion des commentaires 

		//ajout commentaire
		public function addComment(){
			$this->db->set('id', $this->id)
		    		 ->set('id_abonne', $this->id_abonne)
		    	     ->set('id_article', $this->id_article)
		    	     ->set('contenu', $this->contenu)
		    	     ->set('date_time', $this->date_time)
				     ->insert($this->table);
		}

		//trouver les commentaires
		public function findAllCommentBd(){
			$data = $this->db->select('id,id_abonne,id_article,contenu,date_time')
								->from($this->table)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['id_abonne']=$row->id_abonne;
			       	$donnees[$i]['id_article']=$row->id_article;
			       	$donnees[$i]['contenu']=$row->contenu;
			       	$donnees[$i]['date_time']=$row->date_time;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;
		}

		// fonction qui permet de supprimer un comment

			public function suppComment($id){
				$this->db->where('id', $id)
						 ->delete($this->table);
			}




		public function findAllCommentaire($id_article){

			$data = $this->db->select('id,id_article,id_abonne,contenu')
								->from($this->table)
								->where(array('id_article'=>$id_article))
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			   		$donnees[$i]['id_article']=$row->id_article;
			   		$donnees[$i]['id_abonne']=$row->id_abonne;
			       	$donnees[$i]['contenu']=$row->contenu;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;
		}




	// definition des getteurs et des setteurs


		// setteurs

		public function setId($id){
			$this->id = $id;
		}

		public function setId_abonne($id_abonne){
			$this->id_abonne =$id_abonne;
		}

		public function setContenu($contenu){
			$this->contenu =$contenu;
		}

		public function setId_article($id_article){
			$this->id_article =$id_article;
		}

		public function setDate_time($date_time){
			$this->date_time =$date_time;
		}


		// getteurs

		public function getId(){
			return $this->id;
		}

		public function getId_abonne(){
			return $this->id_abonne;
		}

		public function getContenu(){
			return $this->contenu;
		}

		public function getId_article(){
			return $this->id_article;
		}


		public function getDate_time(){
			return $this->date_time;
		}


}		