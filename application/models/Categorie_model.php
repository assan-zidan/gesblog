<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Categorie_model extends CI_Model{
	function __construct()
		{
		
		}

		private $id;
		private $nom;
		private $image;
		// private $nombre;
		private $description;
		private $date_time;
		
		protected $table = 'categorie';

		public function hydrate(array $donnees){
			foreach ($donnees as $key => $value) {
				$method = 'set' .
					ucfirst($key);
				if (method_exists($this, $method)) {
					$this->$method($value);
				}
			}
		}


		public function compte($where = array()){
			return (int) $this->db->where($where)->count_all_results($this->table);
		}


		public function findCategorieInfos($id){
				$data =$this->db->select('nom,image,description,date_time')
						->from($this->table)
						->where(array('id'=>$id))
						->limit(1)
						->get()
						->result();

				$donnees['data']='non';			
				foreach ($data as $row){
			       	
			       	$donnees['nom']=$row->nom;
			       	$donnees['image']=$row->image;
			       	// $donnees['nombre']=$row->nombre;
			       	$donnees['description']=$row->description;
			       	$donnees['date_time']=$row->date_time;
			       	$donnees['data']='ok';
				}

				return $donnees;
		}

		public function findCategorieInfo($nom){
				$data =$this->db->select('id')
						->from($this->table)
						->where(array('nom'=>$nom))
						->limit(1)
						->get()
						->result();

				$donnees['data']='non';			
				foreach ($data as $row){
			       	
			       	$donnees['id']=$row->id;
			       	$donnees['image']=$row->image;
			       	// $donnees['nombre']=$row->nombre;
			       	$donnees['description']=$row->description;
			       	$donnees['date_time']=$row->date_time;
			       	$donnees['data']='ok';
				}

				return $donnees;
		}

			public function addCategories(){

		    $this->db->set('id', $this->id)
		    	->set('nom', $this->nom)
		    	->set('image', $this->image)
		    	// ->set('nombre', $this->nombre)
		    	->set('description', $this->description)
		    	->set('date_time', $this->date_time)
				->insert($this->table);		
		}

		// fonction qui permet de supprimer une catégorie

			public function suppCategorie($id){
				$this->db->where('id', $id)
						 ->delete($this->table);
			}




			public function modCategories($id){

		    $this->db->set('nom', $this->nom)
		    		->set('image', $this->image)
		    		->set('description', $this->description)
		    		->set('date_time', $this->date_time)
					->where('id',$id)
					->update($this->table);		
		}

			// public function addCategorie(){

		 //    $this->db->set('id', $this->id)
		 //    	->set('nom', $this->nom)
		 //    	->set('image', $this->image)
		 //    	->set('nombre', $this->nombre)
		 //    	->set('description', $this->description)
		 //    	->set('date_time', $this->date_time)
			// 	->insert($this->table);
			// $data =$this->db->select('id')
			// 				->from($this->table)
			// 				->where('nom', $this->nom)
			// 				->limit(1)
			// 				->get()
			// 				->result();
			// 	$donnees=array();
			// 	$donnees['data']="non";
			// 	foreach ($data as $row){
			//        	$donnees['id']=$row->id;
			//        	$donnees['data']="ok";
			// 	}

			// return $donnees['id'];
	
			// }


			public function findAllCategorieBd(){
				$data = $this->db->select('id,nom,image,description,date_time')
								->from($this->table)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			   
			       	$donnees[$i]['nom']=$row->nom;
			       	$donnees[$i]['image']=$row->image;
			       	// $donnees[$i]['nombre']=$row->nombre;
			       	$donnees[$i]['description']=$row->description;
			       	$donnees[$i]['date_time']=$row->date_time;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;

			}



			public function findOneCategorie($id){
				$data =$this->db->select('id,nom,image,description,date_time')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();

								
				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			   
			       	$donnees[$i]['nom']=$row->nom;
			       	$donnees[$i]['image']=$row->image;
			       	// $donnees[$i]['nombre']=$row->nombre;
			       	$donnees[$i]['description']=$row->description;
			       	$donnees[$i]['date_time']=$row->date_time;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;
			}


			







	// definition des getteurs et des setteurs


		// setteurs

		public function setId($id){
			$this->id = $id;
		}

		
		public function setNom($nom){
			$this->nom =$nom;
		}

		public function setImage($image){
			$this->image =$image;
		}

		// public function setNombre($nombre){
		// 	$this->nombre =$nombre;
		// }

		public function setDescription($description){
			$this->description =$description;
		}

		public function setDate_time($date_time){
			$this->date_time =$date_time;
		}


		// getteurs

		public function getId(){
			return $this->id;
		}

		


		public function getNom(){
			return $this->nom;
		}

		public function getImage(){
			return $this->image;
		}

		// public function getNombre(){
		// 	return $this->nombre;
		// }


		public function getDescription(){
			return $this->description;
		}


		public function getDate_time(){
			return $this->date_time;
		}


}		