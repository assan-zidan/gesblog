<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Article_model extends CI_Model{
	function __construct()
		{
		
		}

		private $id;
		private $id_categorie;
		private $id_redacteur;
		private $titre;
		private $image;
		private $contenu;
		private $date_time;
		private $nb_like;
		private $etat;
		
		protected $table = 'article';


		public function hydrate(array $donnees){
			foreach ($donnees as $key => $value){
				$method = 'set'.ucfirst($key);
				if (method_exists($this, $method)){
					$this->$method($value);
				}
			}
		}


		public function compte($where = array()){
			return (int) $this->db->where($where)->count_all_results($this->table);
		}



		// recuperer tous les articles


		public function findAllArticleBd(){
				$data = $this->db->select('id,id_categorie,id_redacteur,titre,image,contenu,date_time,nb_like,etat')
								->from($this->table)
								->order_by('id','desc')
								->limit(4)
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['id_categorie']=$row->id_categorie;
			       	$donnees[$i]['id_redacteur']=$row->id_redacteur;
			       	$donnees[$i]['titre']=$row->titre;
			       	$donnees[$i]['image']=$row->image;
			       	$donnees[$i]['contenu']=$row->contenu;
			       	$donnees[$i]['date_time']=$row->date_time;
			       	$donnees[$i]['nb_like']=$row->nb_like;
			       	$donnees[$i]['etat']=$row->etat;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;
			}

		

			public function findArticleInfos($id){
				$data =$this->db->select('id,id_categorie,id_redacteur,titre,image,contenu,date_time,nb_like,etat')
						->from($this->table)
						->where(array('id'=>$id))
						->limit(1)
						->get()
						->result();

				$donnees['data']='non';			
				foreach ($data as $row){
			       	
			       	$donnees['titre']=$row->titre;
			       	$donnees['image']=$row->image;
			       	$donnees['contenu']=$row->contenu;
			       	
			       	$donnees['date_time']=$row->date_time;
			       	$donnees['nb_like']=$row->nb_like;
			       	$donnees['etat']=$row->etat;
			       	$donnees['data']='ok';
				}

				return $donnees;
			}


			// fonction qui recupere les articles en fonction de leur categorie
			public function findArticleCategorie($id_categorie){
				$data =$this->db->select('id,id_categorie,id_redacteur,titre,image,contenu,date_time,nb_like,etat')
						->from($this->table)
						->where(array('id_categorie'=>$id_categorie))
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			   		$donnees[$i]['id_redacteur']=$row->id_redacteur;
			   		$donnees[$i]['id_categorie']=$row->id_categorie;
			       	$donnees[$i]['titre']=$row->titre;
			       	$donnees[$i]['image']=$row->image;
			       	$donnees[$i]['contenu']=$row->contenu;
			       	$donnees[$i]['date_time']=$row->date_time;
			       	$donnees[$i]['nb_like']=$row->nb_like;
			       	$donnees[$i]['etat']=$row->etat;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}




			// public function findAllArticleBd(){
			// 	$data = $this->db->select('id,id_categorie,id_redacteur,titre,image,contenu,date_time,nb_like,etat')
			// 					->from($this->table)
			// 					->order_by('id','desc')
			// 					->get()
			// 					->result();

			// 	$i=0;
			// 	$donnees['data'] = 'non';	
				
			// 	foreach ($data as $row){
			//        	$donnees[$i]['id']=$row->id;
			//    		$donnees[$i]['id_redacteur']=$row->id_redacteur;
			//    		$donnees[$i]['id_categorie']=$row->id_categorie;
			//        	$donnees[$i]['titre']=$row->titre;
			//        	$donnees[$i]['image']=$row->image;
			//        	$donnees[$i]['contenu']=$row->contenu;
			//        	$donnees[$i]['date_time']=$row->date_time;
			//        	$donnees[$i]['nb_like']=$row->nb_like;
			//        	$donnees[$i]['etat']=$row->etat;
			//        	$i++;
			//        	$donnees['data']='ok';
			// 	}
				
			// 	$donnees['total']=$i;
			// 	return $donnees;	
			// }


			// fonction qui donne les information sur un article en fonction de sa categorie
			public function findInfoAllArticleBd($id_article){
				$data = $this->db->select('id,id_categorie,id_redacteur,titre,image,contenu,date_time,nb_like,etat')
								->from($this->table)
								->where(array('id'=>$id_article))
								->limit(1)
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			   		$donnees[$i]['id_redacteur']=$row->id_redacteur;
			   		$donnees[$i]['id_categorie']=$row->id_categorie;
			       	$donnees[$i]['titre']=$row->titre;
			       	$donnees[$i]['image']=$row->image;
			       	$donnees[$i]['contenu']=$row->contenu;
			       	$donnees[$i]['date_time']=$row->date_time;
			       	$donnees[$i]['nb_like']=$row->nb_like;
			       	$donnees[$i]['etat']=$row->etat;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}



			public function addArticles(){

		    	// $this->db->set('id', $this->id)
			    $this->db
			    	// ->set('id_redacteur', $this->id_redacteur)
			    	->set('titre', $this->titre)
			    	->set('image', $this->image)
			    	->set('contenu', $this->contenu)
			    	->set('date_time', $this->date_time)
			    	->set('nb_like', $this->nb_like)
			    	->set('etat', $this->etat)
					->insert($this->table);		
		}



			public function findOneArticle($id){
				$data =$this->db->select('id,id_categorie,id_redacteur,titre,image,contenu,date_time,nb_like,etat')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();

								
				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['id_categorie']=$row->id;
			       	$donnees[$i]['id_redacteur']=$row->id;
			       	$donnees[$i]['titre']=$row->titre;
			       	$donnees[$i]['image']=$row->image;
			       	$donnees[$i]['contenu']=$row->contenu;
			       	$donnees[$i]['date_time']=$row->date_time;
			       	$donnees[$i]['nb_like']=$row->nb_like;
			       	$donnees[$i]['etat']=$row->etat;
			       	
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;
			}

	// fonction qui permet de modififie un article


			public function modArticles($id){

		    $this->db->set('titre', $this->titre)
		    		->set('image', $this->image)
		    		->set('contenu', $this->contenu)
		    		->set('date_time', $this->date_time)
		    		->set('nb_like', $this->nb_like)
		    		->set('etat', $this->etat)
					// ->where('id_redacteur',$id)
					->where('id',$id)
					->update($this->table);		
		}



	// fonction qui permet de trouver les articles d'un redacteur précis

			public function findArticleBd($id){
				$data = $this->db->select('id,id_categorie,id_redacteur,titre,image,contenu,date_time,nb_like,etat')
								->from($this->table)
								->where('id_redacteur',$id)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['id_categorie']=$row->id_categorie;
			       	$donnees[$i]['id_redacteur']=$row->id_redacteur;
			       	$donnees[$i]['titre']=$row->titre;
			       	$donnees[$i]['image']=$row->image;
			       	$donnees[$i]['contenu']=$row->contenu;
			       	$donnees[$i]['date_time']=$row->date_time;
			       	$donnees[$i]['nb_like']=$row->nb_like;
			       	$donnees[$i]['etat']=$row->etat;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}


		// fonction qui permet d'ajouter un article

			public function addArticle(){

			    $this->db->set('id', $this->id)
			    	->set('id_categorie', $this->id_categorie)
			    	->set('id_redacteur', $this->id_redacteur)
			    	->set('titre', $this->titre)
			    	->set('image', $this->image)
			    	->set('contenu', $this->contenu)
			    	->set('date_time', $this->date_time)
			    	->set('nb_like', $this->nb_like)
			    	// ->set('etat', $this->etat)
					->insert($this->table);			
			}

		// fonction qui permet de supprimer un article

			public function suppArticle($id){
				$this->db->where('id', $id)
						 ->delete($this->table);
			}

	// definition des getteurs et des setteurs


		// setteurs

		public function setId($id){
			$this->id = $id;
		}

		public function setId_categorie($id_categorie){
			$this->id_categorie =$id_categorie;
		}

		public function setId_redacteur($id_redacteur){
			$this->id_redacteur =$id_redacteur;
		}

		public function setTitre($titre){
			$this->titre =$titre;
		}

		public function setImage($image){
			$this->image =$image;
		}

		public function setContenu($contenu){
			$this->contenu =$contenu;
		}

		public function setDate_time($date_time){
			$this->date_time =$date_time;
		}

		public function setNb_like($nb_like){
			$this->nb_like =$nb_like;
		}

		public function setEtat($etat){
			$this->etat =$etat;
		}


		// getteurs

		public function getId(){
			return $this->id;
		}

		public function getId_categorie(){
			return $this->id_categorie;
		}

		public function getId_redacteur(){
			return $this->id_redacteur;
		}

		public function getTitre(){
			return $this->titre;
		}

		public function getImage(){
			return $this->image;
		}

		public function getContenu(){
			return $this->contenu;
		}


		public function getDate_time(){
			return $this->date_time;
		}


		public function getNb_like(){
			return $this->nb_like;
		}

		public function getEtat(){
			return $this->etat;
		}





}		