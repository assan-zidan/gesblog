<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>MODERATEUR | IncH Class</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <?php echo admin_bt_css("css/bootstrap.min"); ?>
  <!-- Theme style -->
  <?php echo admin_dist_css("css/AdminLTE.min"); ?>
  <?php echo admin_dist_css("css/skins/_all-skins.min"); ?>
  <!-- iCheck -->
  <?php echo admin_plugins_css("iCheck/flat/blue"); ?>
  <!-- Morris chart -->
  <?php echo admin_plugins_css("morris/morris"); ?>
  <!-- jvectormap -->
  <?php echo admin_plugins_css("jvectormap/jquery-jvectormap-1.2.2"); ?>
  <!-- Date Picker -->
  <?php echo admin_plugins_css("datepicker/datepicker3"); ?>
  <!-- Daterange picker -->
  <?php echo admin_plugins_css("daterangepicker/daterangepicker"); ?>
  <!-- bootstrap wysihtml5 - text editor -->
  <?php echo admin_plugins_css("bootstrap-wysihtml5/bootstrap3-wysihtml5.min"); ?>
  <?php echo css('app/admin_style'); ?>
  <?php 
    echo js('app/jquery.min');
    
    
   ?>
 


  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>