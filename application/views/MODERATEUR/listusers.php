  <div class="content-wrapper">
    <section class="content-header">
        <table id="myTable" class="dataTables_filter table-responsive table-bordered table-striped"> 
          <thead> 
              <th>N°: </th>
              <th> Photo_profil </th>
              <th> Nom </th>
              <th> Status </th>
              <th> Email</th>
              <th> Action</th>
          </thead>
          <tbody> 
            <?php   
              for($i=0; $i<$AllUsers['total']; $i++) {?>
                <tr>  
                  <td> <?php  echo $AllUsers[$i]['id']; ?> </td>
                  <td> <?php echo $AllUsers[$i]['photo_profil']; ?> </td>
                  <td> <?php  echo $AllUsers[$i]['nom']; ?></td>
                  <?php if($AllUsers[$i]['niveau']==3){ ?>
                          <td style="color: orange"> Redacteur </td>
                          <td> <?php echo $AllUsers[$i]['email'];  ?> </td>
                  <?php }elseif($AllUsers[$i]['niveau']==4){ ?>
                          <td> Abonné </td>
                          <td> <?php echo $AllUsers[$i]['email']; ?> </td>
                  <?php }elseif($AllUsers[$i]['niveau']==5){ ?>
                          <td style="color: red">Désactivé</td>
                          <td> <?php echo $AllUsers[$i]['email']; ?> </td> 
                        <?php   } ?>
                  <td>
                    <form method="POST" action="<?php echo site_url(array('Moderation','profileUser')); ?>">
                      <input type="hidden" name="id" value="<?php echo $AllUsers[$i]['id'];  ?>">
                      <input type="hidden" name="niveau" value="<?php echo $AllUsers[$i]['niveau'];  ?>">
                      <input type="hidden" name="nom" value="<?php echo $AllUsers[$i]['nom'];  ?>">
                      <input type="hidden" name="email" value="<?php echo $AllUsers[$i]['email'];  ?>">
                      <input type="hidden" name="photo_profil" value="<?php echo $AllUsers[$i]['photo_profil'];  ?>">
                      <button class="fa fa-filter" type="submit"></button>
                    </form>
                    <form method="POST" action="<?php echo site_url(array('Moderation','AffFormSupUs')); ?>">
                      <input type="hidden" name="id" value="<?php echo $AllUsers[$i]['id'];  ?>">
                      <input type="hidden" name="niveau" value="<?php echo $AllUsers[$i]['niveau'];  ?>">
                      <input type="hidden" name="nom" value="<?php echo $AllUsers[$i]['nom'];  ?>">
                      <input type="hidden" name="email" value="<?php echo $AllUsers[$i]['email'];  ?>">
                      <input type="hidden" name="photo_profil" value="<?php echo $AllUsers[$i]['photo_profil'];  ?>">
                      <?php   if($AllUsers[$i]['niveau']==5) {?>
                      <button class="fa fa fa-lock" type="submit"></button>
                       <?php  } ?> 
                      <?php   if(($AllUsers[$i]['niveau']==3) OR ($AllUsers[$i]['niveau']==4)) {?>
                      <button class="fa fa fa-unlock" type="submit"></button>
                       <?php  } ?> 
                    </form>
                  </td>
                </tr>
            <?php } ?>
          </tbody>
        </table>
    </section>
  </div>