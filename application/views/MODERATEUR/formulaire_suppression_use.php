 <div class="content-wrapper">    
  <section class="content">
    <div class="row">
      <div class="col-md-offset-3 col-md-6" style="border:2px solid blue;height: 600px; border-radius: 10%;">
        <form action="<?php echo site_url(array('Moderation','SupUsers')) ?>" method="post">
            <h3 style="text-align: center;">DESACTIVATION</h3><br>
            <label class="form-label">VOULEZ-VOUS VRAIMENT DESACTIVER ?</label> <br>
            <input type="hidden" name="id" value="<?php echo $id ?>">
            <input type="hidden" name="email" value="<?php echo $email ?>">
            <input type="hidden" name="nom" value="<?php echo $nom ?>">
            <input type="hidden" name="niveau" value="<?php echo $niveau ?>">
            <input type="hidden" name="photo_profil" value="<?php echo $photo_profil ?>">
            <input type="hidden" name="niveau" value="5">
            <button type="submit" class="btn btn-primary btn-block btn-flat" style="width: 50%;">OUI</button>   
        </form>
        <form action="<?php echo site_url(array('Moderation','ListUsers')) ?>" method="post">
          <input type="hidden" name="id" value="<?php echo $id ?>">
          <button type="submit" class="btn btn-primary btn-block btn-flat" style="width: 50%;">NON</button>
        </form>
      </div>
    </div>
  </section>
</div>