<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Welcome </title>
  <link href="//fonts.googleapis.com/css2?family=Barlow:ital,wght@0,300;0,600;0,700;1,400&display=swap" rel="stylesheet">
  <?php echo  css('bootstrap.min');?>
  <?php echo  css('style');?>
  <?php echo  css('owl.carousel.min');?>
  <?php echo css('icon-font.min'); ?>
  <?php echo css('font-awesome'); ?>
  <?php echo css('ionicons.min'); ?>
  <?php echo css('app/admin_style'); ?>
  <?php echo css('app/style'); ?> 
 
</head>

<body>
    <header >
        <section>
            <div class="container-fluid">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="navbar navbar-default wel_navbar">
                        <div class="pull pull-left-1 col-md-5 col-sm-3 col-xs-8">
                            <a class="navbar-brand" href="<?php echo site_url(array('Welcome','index')); ?>"><span class="inch">INCH</span><span class="blog">BLOG</span></a>
                        </div>
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        
                        <div class=" col-md-offset-2 col-md-4  ">
                            <ul class="nav navbar-nav collapse navbar-collapse ">
                                <li><a href="<?php echo site_url(array('Welcome','index')); ?>"><span class="accueil">Accueil</span></a></li>
                                <li><a href="<?php echo site_url(array('Welcome','blog')); ?>"><span class="accueil">Blog</span></a></li>

                                <?php if (isset($_SESSION['ABONNE'])) { ?>
                                <li class="dropdown"><a class="btn btn-default profil_ab" data-toggle="dropdown" href="#" >
                                  <?php 
                                    echo $_SESSION['ABONNE']['nom'];
                                  ?>&nbsp;
                                  <?php echo img($_SESSION['ABONNE']['photo_profil'],'','imge_header') ?> <span class="caret"></span></a>
                                  <ul class="dropdown-menu">
                                      <li><a class="dropdown-item" href="#">Mon profil</a></li>
                                      <li><a class="dropdown-item" href="<?php echo site_url(array('Welcome','deconnexion')); ?>">Deconexion</a></li>
                                  </ul>
                                </li>
                                
                                <?php }else{  ?>

                                <li class="dropdown profil"><a class="btn btn-default" data-toggle="dropdown" href="#" >
                                    Mode anonyme<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="<?php echo site_url(array('Welcome','formulaireConnexion_abonne')); ?>"> Connexion</a></li>
                                    <li><a class="dropdown-item" href="<?php echo site_url(array('Welcome','inscription')); ?>">Creer un Compte</a></li>
                                </ul>
                                </li>
                                <?php } ?>

                            </ul>
                        </div>
                    </div>  
                </div>

                
    

    