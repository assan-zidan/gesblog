    <section class="container-fluid style_footer" >
        <div class="container">
            <div class="col-lg-6 Newsletter_title">
                Abonnez vous à notre Newsletter
            </div>
            <div class="col-lg-6 " style="padding-top: 45px;">
               <div class="form-group">
                   <form action="" method="">
                        <div class="input-group">
                            <input type="email" name="" class="Newsletter_form1" placeholder="veillez entrer votre Email">
                            <input type="submit" value="s'inscrire" class="Newsletter_form" >
                        </div>
                   </form>
               </div> 
            </div>
        </div>
    </section>
    <section class="container">
        <div class="row">
            <div class="col-lg-4">
                <h3>Navigation</h3>
            
                <p><a href="<?php site_url(array('Welcome','index')) ?>"><span class="footer_link">Accueil</span></a></p>
                <p><a href="<?php site_url(array('Welcome','blog')) ?>"><span class="footer_link">Blog</span></a></p>
                <p><a href="<?php site_url(array('Welcome','AllArticle')) ?>"><span class="footer_link">Article</span></a></p>
            
            </div>
            <div class="col-lg-4">
                <h3>liens des reseaux sociaux</h3>
                
                <a href="#facebook" class="btn btn-primary"><span class="fa fa-facebook"></span></a>&nbsp; &nbsp;
                <a href="#twitter" class="btn btn-primary"><span class="fa fa-twitter"></span></a>&nbsp; &nbsp;
                <a href="#instagram" class="btn btn-primary"><span class="fa fa-instagram"></span></a>
            </div>
            <div class="col-lg-4">
                <h3>contact</h3>
                <p>Adresse :567 Rue Joffre, Akwa, Douala, Cameroun.</p>
                <p class="mt-2">Telephone : <a href="tel:+237676233273"> <span class="footer_link">+237676233273</span></a></p>
                <p class="mt-2">Email : <a href="info@inchclass.org"> <span class="footer_link">info@inchclass.org</span></a></p>
            </div>
        </div>
    </section>
    <section>
        <div class="container-fluid Copyright">
            <strong>Copyright &copy; 2021 <a href="" class="footer_link">IncH Class</a>.</strong> All rights
            reserved.
        </div>
    </section>
    
        
        
    

    

    <?php 
        echo js('jquery-3.3.1.min');
        echo js('jquery-3.6.0.min');
        echo js('popper.min');
        echo js('bootstrap.min');
        echo js('owl.carousel.min');
        echo js('main');
        echo js('jquery.waypoints.min');
        echo js('jquery.countup');
        
    ?>

</body>

</html>

