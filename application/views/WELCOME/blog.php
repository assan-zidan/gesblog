	  <section >
      <!-- breadcrumb -->
    </section>
    

    <section class="container">
      <div class="row">
        <h1 class="head"> Categories</h1>
      </div>
      <div class="row"  >
        <?php if ($categorie['data']=="ok") { ?>
          <?php for ($i=0; $i <$categorie['total'] ; $i++) { ?>
            <div class="col-lg-4">
              <div class="cardre_article style" >
                <div>
                  <?php echo img($categorie[$i]['image'],'','cardre_image'); ?>
                </div>
                <h3><?php echo $categorie[$i]['nom'];?></h3>
                <p> <?php echo $categorie[$i]['description']; ?></p>
                <form action="<?php echo site_url(array('Welcome','article')) ?>" method="post" enctype="multipart/form-data" class="form-group">
                  <input type="hidden" name="id" value="<?php echo $categorie[$i]['id']; ?>">
                  <button type="submit" class="btn btn-primary">En savoir plus</button>
                </form>
              </div>
            </div>
          <?php } ?>
        <?php } ?>
      </div>
    </section>
    <!-- fin de section -->
