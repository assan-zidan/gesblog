		
		<section>
			<!-- breadcrumb -->
		</section>
		<section>
			<div class="container">
				<div class="row ">
					<div class="col-lg-12 col-xs-12">
						<div class="row">
							<h1 style="font-weight: bold;color: indigo; font-size: 40px"><?php echo $article_categorie['0']['titre']; ?></h1>
						</div>
						<div class="row ">
							<?php echo img($article_categorie['0']['image'],'','wel_padding'); ?>
						</div>
						<div class="row" style="text-align:justify; padding-top: 30px; padding-bottom: 25px;">
							<?php echo $article_categorie['0']['contenu']; ?>
						</div>
						<div class="row">
							<h3>Commentaire</h3>
						</div>
						<div class="row">
							<?php for ($i=0; $i < $commentaire['total'] ; $i++) { ?>
								<div class="col-lg-8">
									<div class="row cadre_commentaire">
											<div class="col-lg-3">
												<div class="row">
													<?php echo img($AbonneInfos[$i]['photo_profil'],'','comment_profil') ;?>
												</div>
												<div class="row">
													<?php echo $AbonneInfos[$i]['nom'] ;?>
												</div>
											</div>
											<div class="col-lg-7">
												<?php echo $commentaire[$i]['contenu'] ;?>
											</div>
									</div>
								</div>
							<?php } ?>
						</div>
						<div class="row">
							<div class="form-group">
								<form action="<?php echo site_url(array('Welcome','article_complet')) ?>" method="post">
									<div class="form-group">
										<div class="input-group">
											<input type="text" name="contenu" placeholder="donner votre avis" required="">
											 <input type="hidden" name="id_abonne" value="<?php if(isset($_SESSION['ABONNE'])){ 
												echo $_SESSION['ABONNE']['id'];
												// print_r($_SESSION['ABONNE']);
											}
											  ?>">
											
											<input type="hidden" name="id_article" value="<?php echo $article_categorie['0']['id']; ?>">
											<input type="hidden" name="statut" value="2">
											<input type="submit" name="" value="Commenter">
										</div>
									</div>
									
								</form>
							</div>
						</div>

					</div>
				</div>
			</div>
			
		</section>