        <div class="mediar_header">
          
        </div>
      </div>
    </section style="background-color: white;">   
  </header>
  <!-- fin du header -->
  <!-- section de nos informations -->
    <section class="">
      <div class="container ">
        <div class="row">
          <div class="col-lg-6">
            <div class="row "> 
              <?php  echo img('background12.jpg','','wel_padding img-rounded');?>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="row">
              <h1 style="font-weight: bold;color: indigo; font-size: 40px">Qui sommes nous</h1>
            </div>
            <div class="row texto">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- fin de section -->
    <!-- section des articles preferes  -->
    <section class="container">
      <div class="row">
        <h1 class="head" style="font-weight: bold;color: indigo; font-size: 40px"> Articles recents</h1>
      </div>
      <div class="row" style=" border-radius: 20px">
          <?php if ($article['data']=="ok"); { ?>
            <?php for ($i=0; $i < $article['total']  ; $i++) { ?>
              <div class="col-lg-3">
                    <div class="cardre_article style">
                    <div class="">
                      <?php echo img($article[$i]['image'],'','cardre_image img-rounded'); ?>
                      
                    </div>
                    <h3><?php echo $article[$i]['titre']; ?></h3>
                    <form action="<?php echo site_url(array('Welcome','article_complet')) ?>" method="post" enctype="multipart/form-data" class="form-group">
                        <input type="hidden" name="id" value="<?php echo $article[$i]['id']; ?>">
                        <input type="hidden" name="statut" value="1">
                       
                        <button type="submit" class="btn btn-primary">Lire plus</button>

                      </form>
               
                    </div>
                </div>
            <?php } ?>
                
          <?php } ?>
        </div>
    </section>
    <!-- fin de section -->


    <!-- section des categories  -->
    <section  class="container">
      <div class="row">
        <h1 class="head" style="font-weight: bold ;color: indigo ;font-size: 40px"> Categories</h1>
      </div>
      <div style="  border-radius: 20px; " class="row">
        <?php if ($categorie['data']=="ok") { ?>
          <?php for ($i=0; $i <$categorie['total'] ; $i++) { ?>
            <div class="col-lg-4">
              <div class="cardre_categorie style">
                <div>
                  <?php echo img($categorie[$i]['image'],'','cardre_image'); ?>
                </div>
                <h3><?php echo $categorie[$i]['nom'];?></h3>
                <p> <?php echo $categorie[$i]['description']; ?></p>
                <form action="<?php echo site_url(array('Welcome','article')) ?>" method="post" enctype="multipart/form-data" class="form-group">
                  <input type="hidden" name="id" value="<?php echo $categorie[$i]['id']; ?>">
                  <button type="submit" class="btn btn-primary">En savoir plus</button>
                </form>
              </div>
            </div>
          <?php } ?>
        <?php } ?>
      </div>
    </section>
    <!-- fin de section -->


    <!-- section des redacteurs-->
    <!-- <section class="container-fluid mediar_header">
      <div class="row">
        <h1 style="font-weight: bold ;color: indigo ;font-size: 40px"> Nos Redacteur</h1>
      </div>
      <div class="row">
        <?php for ($i=0; $i <$redacteur['total'] ; $i++) { ?>
          <div class="col-lg-4">
            <div class="cardre_redac">
              <div class="row ">
                <?php echo img($redacteur[$i]['photo_profil'],'','image_title') ;?>
              </div>
              <div class="row">
                <h3><?php echo $redacteur[$i]['nom'] ?></h3>
              </div>
            </div>
          </div>
          
        <?php } ?>
      </div>
    </section> -->
    <!-- fin de section -->


    

  