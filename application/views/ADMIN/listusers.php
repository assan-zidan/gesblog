  <div class="content-wrapper">
    <section class="content-header">
        <table id="myTable" class="dataTables_filter table-responsive table-bordered table-striped"> 
          <thead> 
              <th>N°: </th>
              <th> Photo_profil </th>
              <th> Nom </th>
              <th> Status </th>
              <th> Email</th>
              <th> Action</th>
          </thead>
          <tbody> 
            <?php   
            if ($AllUsers['data']=='ok') {
              for($i=0; $i<$AllUsers['total']; $i++) {?>
                <tr>  
                  <td> <?php  echo $AllUsers[$i]['id']; ?> </td>
                  <td> <?php echo $AllUsers[$i]['photo_profil']; ?> </td>
                  <td> <?php  echo $AllUsers[$i]['nom']; ?></td>
                  <?php if($AllUsers[$i]['niveau']==1){ ?>
                          <td style="color: green"> Administrateur </td>
                          <td> <?php  echo $AllUsers[$i]['email']; ?> </td>
                  <?php }elseif($AllUsers[$i]['niveau']==2){ ?>
                          <td style="color: blue"> Modérateur </td>
                          <td> <?php  echo $AllUsers[$i]['email']; ?> </td>
                  <?php }elseif($AllUsers[$i]['niveau']==3){ ?>
                          <td style="color: orange"> Redacteur </td>
                          <td> <?php echo $AllUsers[$i]['email'];  ?> </td>
                  <?php }elseif($AllUsers[$i]['niveau']==4){ ?>
                          <td> Abonné </td>
                          <td> <?php echo $AllUsers[$i]['email']; ?> </td>
                  <?php }else{ ?>
                          <td style="color: red"> Supprimé</td>
                          <td> <?php echo $AllUsers[$i]['email']; ?> </td> 
                        <?php   } ?>
                  <td>
                    <form method="POST" action="<?php echo site_url(array('Administration','profileUser')); ?>">
                      <input type="hidden" name="id" value="<?php echo $AllUsers[$i]['id'];  ?>">
                       <input type="hidden" name="niveau" value="<?php echo $AllUsers[$i]['niveau'];  ?>">
                      <input type="hidden" name="nom" value="<?php echo $AllUsers[$i]['nom'];  ?>">
                      <input type="hidden" name="email" value="<?php echo $AllUsers[$i]['email'];  ?>">
                      <input type="hidden" name="photo_profil" value="<?php echo $AllUsers[$i]['photo_profil'];  ?>">

                      <button class="fa fa-filter" type="submit"></button>
                    </form>
                    <form method="POST" action="<?php echo site_url(array('Administration','AffFormSupUs')); ?>">
                      <input type="hidden" name="id" value="<?php echo $AllUsers[$i]['id'];  ?>">
                      <input type="hidden" name="niveau" value="<?php echo $AllUsers[$i]['niveau'];  ?>">
                      <input type="hidden" name="nom" value="<?php echo $AllUsers[$i]['nom'];  ?>">
                      <input type="hidden" name="email" value="<?php echo $AllUsers[$i]['email'];  ?>">
                      <input type="hidden" name="photo_profil" value="<?php echo $AllUsers[$i]['photo_profil'];  ?>">
                      <button class="fa fa fa-trash-o" type="submit"></button>
                    </form>
                  </td>
                </tr>
              <?php } ?>
            <?php } ?>
          </tbody>
        </table>
    </section>
  </div>