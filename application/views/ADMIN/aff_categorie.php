
<div class="content-wrapper">
    
    <section class="content">
        <div class="row">
            <div class="col-md-10 col-md-push-1 col-sm-8 c0l-sm-push-1" style="border: 2px solid rgb(0,131,143); background-color: white; margin-top: 50px;">
                    <h2 style="text-align: center;"> <span>Categorie</span></h2>
                    
                
                <form action="<?php echo site_url(array('Administration','ModCategories')) ?>" method="post" enctype="multipart/form-data" class="" style=" margin-top: 20px;">

                    <div class="col-md-12" style="margin-top:10px; margin-left: 0px; ">
                        <label > 
                            <label for="nom" class="btn btn-xs btn-default pull-right indicate">
                                
                            </label> 
                            <span class="">Nom </span><span class="fa fa-pencil "></span> 
                           
                            <input class="form-control" id="nom" style="background-color: inherit; margin-top: 10px; margin-bottom: 10px; width: 350%" type="text" name="nom" value="<?php echo $AllCategorie['0']['nom'] ?>">
                                
                            
                        </label> <br>
                    </div>

                    <div class="col-md-12" style="margin-top: 10px; margin-left: 0px;">
                        <label> 
                            <label for="description" class="btn btn-xs btn-default pull-right indicate">
                                
                            </label> 
                            <span class="">Description</span> <span class="fa fa-pencil"></span>
                            <textarea class="form-control" id="local-upload" style=" margin-top: 10px; margin-bottom: 10px;" type="text" rows="30" cols="20" name="description" value="">
                                <?php echo $AllCategorie['0']['description']?>
                            </textarea>
                            
                            <input type="hidden" name="id" value="<?php echo $AllCategorie['0']['id'] ?>">
                             <!-- <textarea  name="contenu" id="local-upload" rows="30" cols="10"></textarea><br> -->
                        </label>
                    </div>
                    <div class="col-md-12" style="margin-left: 0px; ">
                        
                            <label for="nom" class="btn btn-xs btn-default pull-right indicate">
                        
                            </label>

                            
                            <label class="form-label"style="width: 350%">Telecharger votre photo de pofil<span class="fa fa-pencil"></span></label>
                         <!--    <input  type="file" name="image" value="" >
                            <input class="form-control" type="file" name="photo_profil" accept="image/*" onchange="loadFile(event)" style="width: 70%"> -->
                            <div>
                                <input class="form-control" type="file" name="image" accept="image/*" onchange="loadFile(event)" style="width: 70%">
                                <img  accept="image/*" onchange="loadFile(event)" id="im" class="img_visualise" src="<?php echo img_url($AllCategorie['0']['image'])?>" style='width:50%; height:140px; ' >
                            <!-- <div class="row imge"><img id="im"   src="<?php echo img_url($_SESSION['ADMIN']['photo_profil']); ?>" style='width:100px; height:100px; border-radius:50%; margin-top: -150px; margin-left: 120px;'/>
                    
                            </div> -->
                            </div>

                    </div>
      
                    <div class="col-md-4">
                        <input class="form-control" type="reset" value="Annuler" class="btn btn-default btn-sm" style="margin-bottom: 20px; margin-left: 150px; background-color: rgb(0,131,143);margin-top: 20px; color: white"> 
                    </div> 
                    <div class="col-md-4 col-md-push-2">  
                        <input class="form-control" type="submit" value="modifier" class="btn btn-default btn-sm" style="margin-bottom: 20px; background-color: rgb(0,131,143); margin-left: 50px;margin-top: 20px; color: white"> 
                    </div>
                                      
                </form>
                <div class=" col-md-4"><!-- <div class="editer btn btn-default" id="edit" style=""></div> -->
                <!-- <button class="editer btn btn-default" id="edit" data-toggle="modal" data-target="#myModal" type="submit">Modifier</button> -->
                </div>
            </div>
        </div>


    </section> 
</div>

 <script src="https://cdn.tiny.cloud/1/5r3678jn7snbdpw0u4zvt2zth82bm2nwio7sula0k37hnrp0/tinymce/5/tinymce.min.js"></script>
    <script type="text/javascript">
      var base_url = {
        'url' : 'http://localhost/k-immofinanz/',
        'author' : 'Cyprien DONTSA'
      };
      var finalUrl = base_url.url+'Administration/upload';
      tinymce.init({
          selector: 'textarea#local-upload',
          plugins: 'image code searchreplace wordcount visualblocks visualchars fullscreen insertdatetime media nonbreaking contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker codesample',
          toolbar1: 'undo redo | newdocument | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect',
          toolbar2: "cut copy paste  | searchreplace | bullist numlist | outdent indent | link unlink   | insertdatetime preview | forecolor backcolor | table | hr removeformat | subscript superscript   |  fullscreen | ltr rtl | image code |codesample print | contextmenu",

        /* without images_upload_url set, Upload tab won't show up*/
          images_upload_url: 'finalUrl',

        /* we override default upload handler to simulate successful upload*/
          images_upload_handler: function (blobInfo, success, failure) {
            var xhr, formData;
            
            xhr = new XMLHttpRequest();
            xhr.withCredentials = false;
            xhr.open('POST',finalUrl);
          
            xhr.onload = function() {
                var json;
            
                if (xhr.status != 200) {
                    failure('HTTP Error: ' + xhr.status);
                    return;
                }
            
                json = JSON.parse(xhr.responseText);
            
                if (!json || typeof json.location != 'string') {
                    failure('Invalid JSON: ' + xhr.responseText);
                    return;
                }
            
                success(json.location);
            };
          
            formData = new FormData();
            formData.append('file', blobInfo.blob(), blobInfo.filename());
          
            xhr.send(formData);
        },
      });
    </script>