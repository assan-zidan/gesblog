
<div class="content-wrapper">

    <section class="content">
         <div class="col-md-10 col-md-push-1 col-sm-8 col-sm-push-1" style="border: 2px solid aqua;">
                <h2 style="text-align: center;"> <span>Informations &nbsp Personnelles</span></h2>
                <!-- <?php //print_r($_SESSION['ADMIN'] ) ?>  -->
            
            <form action="<?php echo site_url(array('Administration','ModAd')) ?>" method="post" enctype="multipart/form-data" style=" margin-top: 20px;">
                <div class="col-md-12" style="margin-left: 0px;">
                    <label for="new_image">
                        <label for="nom" class="btn btn-xs btn-default pull-right indicate">
                            <span class="fa fa-pencil"></span>
                        </label> 
                        <img id="image_depart"  class="img_visualise" src="<?php echo img_url($_SESSION['ADMIN']['photo_profil']); ?>" style='width:140px; height:140px; border-radius:50%' >
                    </label>
                </div>
                <div class="col-md-12" style="margin-top: 10px; margin-left: 0px;">
                    <label > 
                        <label for="nom" class="btn btn-xs btn-default pull-right indicate">
                            <span class="fa fa-pencil"></span>
                        </label> 
                        <span class="">Nom </span> 
                        <input class="form-control" id="nom" style="background-color: inherit; border: none; margin-top: 10px; margin-bottom: 10px;" type="text" name="nom" value=<?php echo $_SESSION['ADMIN']['nom'] ?>>
                    </label> <br>
                </div>

                <div class="col-md-12" style="margin-top: 10px; margin-left: 0px;">
                    <label> 
                        <label for="email" class="btn btn-xs btn-default pull-right indicate">
                            <span class="fa fa-pencil"></span>
                        </label> 
                        <span class="">Email </span> 
                        <input class="form-control" id="email" style="background-color: inherit; border: none; margin-top: 10px; margin-bottom: 10px;"  type="email" name="email" value=<?php echo $_SESSION['ADMIN']['email']?>>
                    </label>
                </div>
                <div class="col-md-12" style="margin-top: 10px; margin-left: 0px;">
                    <label> 
                        <label for="pwd" class="btn btn-xs btn-default pull-right indicate">
                            <span class="fa fa-pencil"></span>
                        </label> 
                        <span class="">Pwd </span> 
                        <input class="form-control" id="pwd" style="background-color: inherit; border: none; margin-top: 10px; margin-bottom: 10px;" type="password" name="pwd" placeholder="Mot de passe" value=<?php echo $_SESSION['ADMIN']['pwd']?>>
                    </label><br>
                </div>
                <div class="col-md-4 ">  
                    <input class="form-control" type="submit" value="Modifier" class="btn btn-default btn-sm" style="margin-bottom: 20px; background-color: aqua; margin-left: 50px;"> 
                </div>
            </form>
            <form action="<?php echo site_url(array('Administration','index')) ?>" method="post" enctype="multipart/form-data" style=" margin-top: 20px;">
                <div class="col-md-4 col-md-push-2">
                    <input class="form-control" type="submit" value="Annuler" class="btn btn-default btn-sm" style="margin-bottom: 20px; margin-left: 200px; background-color: aqua;">   
                </div>                  
            </form>
        </div>

    </section>
</div>