
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <!-- Dashboard -->
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class=""></i> </a></li>
         
      </ol>
    </section>

    <!-- Main content -->
    
    <section class="content" style="background-color: white">
      <div class="row">
        <h2 style="text-align: center; color: blue;"> <span>AJOUT CATEGORIE</span></h2> <br> 
        <div class="col-lg-offset-3 col-lg-6" style="width: 70%;margin-left: 200px">
          <form action="<?php echo site_url(array('Administration','AddCategories')) ?>" method="post"  enctype="multipart/form-data" >
         
              <label class="form-label">Nom de la categorie</label>
              <input class="form-control" type="text" name="nom" placeholder="veuillez entrer le nom de la categorie " style=" "><br>
              <input type="hidden" name="date_time" value="<?php echo date('d/m/y h:i:s ') ?>">
              <!-- <input type="hidden" name="nombre" value="0"> -->
              
              <label>Description</label><br>
              <textarea  name="description" id="local-upload" rows="30" cols="10"></textarea><br>
              
              <label class="form-label">Telecharger une image</label>
              <input class="form-control" type="file" name="image" accept="image/*" onchange="loadFile(event)" style="">
              <div>
                <div class="row imge"><img id="im"/></div>
              </div>
              <div class="col-md-3">
                <button type="submit" class="btn btn-primary btn-block btn-flat" style="margin-top: 80px;margin-left: ;">Enregistrer</button>
              </div>
          </form>
          <div class="col-md-3">  
            <form action="<?php echo site_url(array('Administration','ListCategorie')) ?>" method="post"  enctype="multipart/form-data">
            <button type="submit" class="btn btn-primary btn-block btn-flat"style="margin-top: 80px;margin-left: 400px; ">Annuler</button>  
            </form>
          </div> 
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>


  <script src="https://cdn.tiny.cloud/1/5r3678jn7snbdpw0u4zvt2zth82bm2nwio7sula0k37hnrp0/tinymce/5/tinymce.min.js"></script>
    <script type="text/javascript">
      var base_url = {
        'url' : 'http://localhost/k-immofinanz/',
        'author' : 'Cyprien DONTSA'
      };
      var finalUrl = base_url.url+'Administration/upload';
      tinymce.init({
          selector: 'textarea#local-upload',
          plugins: 'image code searchreplace wordcount visualblocks visualchars fullscreen insertdatetime media nonbreaking contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker codesample',
          toolbar1: 'undo redo | newdocument | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect',
          toolbar2: "cut copy paste  | searchreplace | bullist numlist | outdent indent | link unlink   | insertdatetime preview | forecolor backcolor | table | hr removeformat | subscript superscript   |  fullscreen | ltr rtl | image code |codesample print | contextmenu",

        /* without images_upload_url set, Upload tab won't show up*/
          images_upload_url: 'finalUrl',

        /* we override default upload handler to simulate successful upload*/
          images_upload_handler: function (blobInfo, success, failure) {
            var xhr, formData;
            
            xhr = new XMLHttpRequest();
            xhr.withCredentials = false;
            xhr.open('POST',finalUrl);
          
            xhr.onload = function() {
                var json;
            
                if (xhr.status != 200) {
                    failure('HTTP Error: ' + xhr.status);
                    return;
                }
            
                json = JSON.parse(xhr.responseText);
            
                if (!json || typeof json.location != 'string') {
                    failure('Invalid JSON: ' + xhr.responseText);
                    return;
                }
            
                success(json.location);
            };
          
            formData = new FormData();
            formData.append('file', blobInfo.blob(), blobInfo.filename());
          
            xhr.send(formData);
        },
      });
    </script>
  
















































