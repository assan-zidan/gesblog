
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <!-- Dashboard -->
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class=""></i> </a></li>
         
      </ol>
    </section>

    <!-- Main content -->
    
    <section class="content">
      <div class="row">
        <div class="col-lg-offset-3 col-lg-6">
          <h1>Formulaire d'ajout d'un utilisateur</h1><br>
          <form action="<?php echo site_url(array('Administration','addUsers')) ?>" method="post" style="border:2px solid blue;height: 600px; border-radius: 10%;" enctype="multipart/form-data">


            <div class="form-group" style="margin-left: 130px;">
              <br>  
              <br>  
              <br>  
              <label>Choisir d'un acteur</label>
              <select class="form-control" name="niveau" style="width: 70%;"
              >
                <option>Choix de l'utilisateur</option>

                <option value="1"><?php echo "Administrateur" ?></option>
                <option value="2"><?php echo "Moderateur" ?></option>
                <option value="3"><?php echo "Redacteur" ?></option>
              </select><br>
              <label class="form-label">Nom</label>

              <input class="form-control" type="text" name="nom" placeholder="veuillez entrer votre nom " style="width: 70%;"><br>
              <label class="form-label">Email</label>
              <input class="form-control" type="email" name="email" placeholder="veuillez entrer votre Email " style="width: 70%;"><br>
              <label class="form-label">Mot de passe</label>
              <input class="form-control" type="password" name="pwd" placeholder="veuillez entrer votre Mot de passe " style="width: 70%;"><br>

              <label class="form-label">Telecharger votre photo de pofil</label>
              <input class="form-control" type="file" name="photo_profil" accept="image/*" onchange="loadFile(event)" style="width: 70%"><br><br><br><br><br><br><br><br>
              <div>
                <div class="row imge"><img id="im" class="img_visualise"  src="<?php echo img_url($_SESSION['ADMIN']['photo_profil']); ?>" style='width:100px; height:100px; border-radius:50%; margin-top: -150px; margin-left: 120px;'/>
                
                </div>
              </div>
              <div class="col-xs-12">
              <button type="submit" class="btn btn-primary btn-block btn-flat" style="width: 50%;">Continuer</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
    
    <!-- /.content -->
  </div>
