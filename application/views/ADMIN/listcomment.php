
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <!-- Dashboard -->
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class=""></i></a></li>   
      </ol>
    </section>

    <!-- Main content -->
    
      <!-- Small boxes (Stat box) -->
    <section class="content-header">
        <h2 style="text-align: center; color: blue;"> <span>LISTE DES COMMENTAIRES</span></h2>       
        <table id="myTable" class="dataTables_filter table-responsive table-bordered table-striped"> 
          <thead> 
            <th>N°: </th>
            <th> ID ABONNE </th>
            <th> ID ARTICLE </th>
            <th> CONTENU</th>
            <th> DATE</th>
            <th> ACTION</th>
          </thead>
          
          <tbody> 
              <?php   
               if ($AllComment['data']=='ok') {
                    for($i=0; $i<$AllComment['total']; $i++) {?>
              <tr>  
                <td> <?php  echo $AllComment[$i]['id']; ?> </td>
                <td> <?php  echo $AllComment[$i]['id_abonne']; ?> </td>
                <td> <?php  echo $AllComment[$i]['id_article']; ?></td>
                <td> <?php  echo $AllComment[$i]['contenu']; ?></td>
                <td> <?php  echo $AllComment[$i]['date_time']; ?></td>
                <td>
                  <!-- <form method="POST" action="<?php //echo site_url(array('Administration','AffArticle')); ?>">
                    <input type="hidden" name="id" value="<?php echo $AllComment[$i]['id'];  ?>">
                    <button class="fa fa-pencil" type="submit" style="display: inline-block;" alt="editer" ></button>
                  </form> -->
                  <form method="POST" action="<?php echo site_url(array('Administration','AffFormSupCom')); ?>">
                    <input type="hidden" name="id" value="<?php echo $AllComment[$i]['id'];  ?>">
                    <button class="fa fa fa-trash-o" alt="editer"><span  data-toggle="modal" href="#infos"></span></button>    
                  </form>
                </td>
              </tr>
            <?php } }else{ } ?>
          </tbody>
      </table>
    </section>
    
    <!-- /.content -->
  </div>