<aside class="main-sidebar">
  <section class="sidebar">
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li><a href="<?php echo site_url(array('Administration','index')); ?>"><i class="fa fa-circle-o text-red"></i> <span> HOME</span></a></li>
      
      <li class="treeview  menu-open">
        <a href="#">
          <i class="fa fa-users"></i>
          <span>Gestion des Membres</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo site_url(array('Administration','AddUser')); ?>"><i class="ion-android-add-circle"></i>  Ajouter un utilisateur</a></li>
          <li><a href="<?php echo site_url(array('Administration','ListUsers')); ?>"><i class="fa fa-star"></i>Liste des Users</a></li>
        </ul>
      </li>

      <li class="treeview memu-open">
        <a href="#">
          <span>Gestion des Categories</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>

        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo site_url(array('Administration','ListCategorie')); ?>"><i class="ion-android-add-circle"></i> Consulter</a></li>
        </ul>
      </li>


      <li class="treeview memu-open">
        <a href="#">
          <span>Gestion des Articles</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo site_url(array('Administration','ListArticle')); ?>"><i class="ion-android-add-circle"></i> Consulter</a></li>
        </ul>

      </li>

      <li class="treeview memu-open">
        <a href="#">
          <span>Gestion des Commentaires</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo site_url(array('Administration','ListComment')); ?>"><i class="ion-android-add-circle"></i> Consulter</a></li>
        </ul>

      </li>

     <!--  <li class="treeview memu-open">
        <a href="#">
          <span>Gestion des Liens</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo site_url(array('Administration','AddUser')); ?>"><i class="ion-android-add-circle"></i> Consulter</a></li>
        </ul>

      </li> -->
    
    </ul>
  </section>
</aside>