  
  
<aside class="main-sidebar">
  <section class="sidebar">
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">NAVIGATION MODERATEUR</li>
      <li><a href="<?php echo site_url(array('Moderation','index')); ?>"><i class="fa fa-circle-o text-red"></i> <span> Home</span></a></li>
      
      <li class="treeview  menu-open">
        <a href="#">
          <i class="fa fa-users"></i>
          <span>Gestion des membres</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <!-- <li><a href="<?php echo site_url(array('Moderation',)); ?>"><i class="ion-android-add-circle"></i>  Ajouter un moderateur</a></li>
          <li><a href="<?php echo site_url(array('Moderation','')); ?>"><i class="ion-android-add-circle"></i>  Ajouter un redacteur</a></li>
          <li><a href="<?php echo site_url(array('Moderation','')); ?>"><i class="ion-android-add-circle"></i>  Ajouter un abonne</a></li> -->
          <li><a href="<?php echo site_url(array('Moderation','ListUsers')); ?>"><i class="fa fa-star"></i>Liste des Users</a></li>
        </ul>
      </li>

      <li class="treeview memu-open">
        <a href="#">
          <span>Gestion des categories</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>

        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo site_url(array('Moderation','ListCategorie')); ?>"><i class="ion-android-add-circle"></i> Consulter</a></li>
        </ul>
      </li>


      <li class="treeview memu-open">
        <a href="#">
          <span>Gestion des articles</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo site_url(array('Moderation','ListArticle')); ?>"><i class="ion-android-add-circle"></i> Consulter</a></li>
        </ul>

      </li>

      <li class="treeview memu-open">
        <a href="#">
          <span>Gestion des commentaires</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo site_url(array('Moderation','ListComment')); ?>"><i class="ion-android-add-circle"></i> Consulter</a></li>
        </ul>

      </li>



    <!--   <li class="treeview memu-open">
        <a href="#">
          <span>Gestion des liens de redirections</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo site_url(array('Administration','#')); ?>"><i class="ion-android-add-circle"></i> Consulter</a></li>
        </ul>

      </li> -->
    
    </ul>
  </section>
</aside>

  
