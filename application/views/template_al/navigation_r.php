  
  
<aside class="main-sidebar">
  <section class="sidebar">
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li><a href="<?php echo site_url(array('Redaction','index')); ?>"><i class="fa fa-circle-o text-red"></i> <span> HOME</span></a></li>

      <li class="treeview memu-open">
        <a href="#">
          <span>GESTION DES ARTICLES</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo site_url(array('Redaction','AddArticle')); ?>"><i class="ion-android-add-circle"></i> &nbsp &nbsp Ajouter</a></li>
          <!-- <li><a href="<?php echo site_url(array('Administration',)); ?>"><i class="ion-android-add-circle"></i>  Ajouter un moderateur</a></li>
          <li><a href="<?php echo site_url(array('Administration','')); ?>"><i class="ion-android-add-circle"></i>  Ajouter un redacteur</a></li>
          <li><a href="<?php echo site_url(array('Administration','')); ?>"><i class="ion-android-add-circle"></i>  Ajouter un abonne</a></li> -->
          <li><a href="<?php echo site_url(array('Redaction','ListArticle')); ?>"><i class="fa fa-star"></i>&nbsp &nbspConsulter</a></li>
        </ul>

      </li>
    
    </ul>
  </section>
</aside>

  
