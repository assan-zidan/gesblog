<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Moderation extends CI_Controller {

			
	public function index(){
		
		 if (isset($_SESSION['MODERATEUR'])) {
			$this->load->view('MODERATEUR/index');
			$this->load->view('template_al/navigation_moderateur');
			$this->load->view('MODERATEUR/home');
			$this->load->view('MODERATEUR/footer');
		
				
		}else{
	   	 session_destroy();
		 redirect(site_url(array('Moderation','formulaireConnexion')));
		}
	}

	// fonction qui charge le formulaire de connexion pour un moderateur

	public function formulaireConnexion(){
		
		if (isset($_SESSION['MODERATEUR'])) {
			if (isset($_SESSION['MODERATEUR'])) {
				redirect(site_url(array('Moderation','index')));
			}else{
				session_destroy();
				redirect(site_url(array('Moderation','formulaireConnexion')));
			}
		}else{
			$this->load->view('gestion_moderateur/formulaire_connexion_m');
		}
	}


	// Gestions des Moderateurs


	public function manageMod(){
		
		if (isset($_SESSION['MODERATEUR'])) {
			
			$data['AllMod']=$this->Moderateur->findAllModBd();
			$this->load->view('WELCOME/index',$data);
			$this->load->view('template_al/navigation');
			$this->load->view('MODERATEUR/home');
			$this->load->view('WELCOME/footer');
			
		}else{
			session_destroy();
			redirect(site_url(array('Moderation','formulaireConnexion')));
		}
	}

	public function testExitMod($email){
        $etat=0;
        $data['infoMod']=$this->Moderateur->findAllModBd();
        if ($data['infoMod']['total']<=0) {
            
        }else{
            for ($i=0; $i <$data['infoMod']['total'] ; $i++) { 
                if ($data['infoMod'][$i]['email']==$email) {
                    $etat=1;
                    break;
                }else{
                    $etat=0;
                }
            }
        }
        return $etat;
	}

	public function addMod(){
		if (isset($_SESSION['MODERATEUR'])) {
			if (isset($_POST)) {
				$etat=$this->testExitMod($_POST['email']);
				if ($etat==0) {
					if (isset($_FILES['profil']) AND $_FILES['profil']['error'] == 0 ){
	        		// Testons si le fichier n'est pas trop gros
	                    if ($_FILES['profil']['size'] <= 100000000){
	                        // Testons si l'extension est autorisée
	                        $infosfichier =pathinfo($_FILES['profil']['name']);
	                        $extension_upload = $infosfichier['extension'];

	                        $config =$_FILES['profil']['name'].date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i').$_SESSION['MODERATEUR']['id'];
	 						$ma_variable = str_replace('.', '_', $config);
							$ma_variable = str_replace(' ', '_', $config);
							$config = $ma_variable.'.'.$extension_upload;
							move_uploaded_file($_FILES['profil']['tmp_name'],'assets/images/user_profil/'.$config);
							$data['photo_profil']=$config;
							
	                    }else{
	                        $data['message_save']="La taille du fichier choisi  est très grande veuillez le remplacer svp !!";
	                        $data['message']='non';
	                    }
	                }else{
	                    $data['message_save']="L'image choisie  est endommagée  veuillez la remplacer svp !!";
	                    $data['message']='non';
	                }
					
					$data['nom']=$_POST['nom'];
					$data['email']=$_POST['email'];
					$data['password']=$_POST['password'];	
					$data['date']=date('Y-m-d H:i:s');
					$this->Moderateur->hydrate($data);
					$this->Moderateur->addMod();
					$_SESSION['message_save']="Modérateur enregistré avec success !!";
			 		$_SESSION['success']='ok';
			 		redirect(site_url(array('Moderation','manageMod')));
					
				}else{
					session_destroy();
					direct(site_url(array('Moderation','formulaireConnexion')));
				}
			}else{
				session_destroy();
				redirect(site_url(array('Moderation','formulaireConnexion')));
			}
		}else{
			session_destroy();
			redirect(site_url(array('Moderation','formulaireConnexion')));
		}

	}

		// fonction qui permet de modifier un admin
		public function ModMo(){
			if (isset($_SESSION['MODERATEUR'])) {
				print_r($_POST);
				$data['email']=$_POST['email'];
				$data['pwd']=$_POST['pwd'];
				$data['nom']=$_POST['nom'];
				$data['id_users']=$_SESSION['MODERATEUR']['id_users'];
				if (($_POST['nom']!=$_SESSION['MODERATEUR']['nom']) OR ($_POST['email']!=$_SESSION['MODERATEUR']['email']) OR ($_POST['pwd']!=$_SESSION['MODERATEUR']['pwd'])) {
					if (($_POST['nom']!=$_SESSION['MODERATEUR']['nom'])) {
						$this->Users->hydrate($data);
						$this->Users->modUser($_SESSION['MODERATEUR']['id_users']);
						$this->Moderateur->hydrate($data);
						$this->Moderateur->modModer();
					} elseif ($_POST['email']!=$_SESSION['MODERATEUR']['email']) {
						$this->Users->hydrate($data);
						$this->Users->modUser($_SESSION['MODERATEUR']['id_users']);
						$this->Moderateur->hydrate($data);
						$this->Moderateur->modModer();
					}elseif ($_POST['pwd']!=$_SESSION['MODERATEUR']['pwd']){
						$this->Users->hydrate($data);
						$this->Users->modUser($_SESSION['MODERATEUR']['id_users']);
						$this->Moderateur->hydrate($data);
						$this->Moderateur->modModer();
					}else{
					$this->Users->hydrate($data);
					$this->Users->modUser();
					$this->Moderateur->hydrate($data);
					$this->Moderateur->modModer();
					}
					redirect(site_url(array('Moderation', 'index')));
				}else{
					$this->Users->hydrate($data);
					$this->Users->modUser();
					$this->Moderateur->hydrate($data);
					$this->Moderateur->modModer();
					redirect(site_url(array('Moderation', 'index')));
					
				}
			}else {
				session_destroy();
				redirect(site_url(array('Moderation', 'formulaireConnexion')));
				}	
		}



	// fonction qui permet a un moderateur de se connecter a sa session
		public function connexion_moderateur(){

			if (isset($_POST['email']) && isset($_POST['pwd'])) {

				$moderateur = $this->Moderateur->findAllModBd();
				for ($i = 0; $i < $moderateur['total']; $i++) {
					if ($moderateur[$i]['email'] == $_POST['email'] && $moderateur[$i]['pwd'] == $_POST['pwd']) {
						$a = $moderateur[$i]['id_users'];
						$data = $this->Users->findUsersInfos($a);
						print_r($data);
						$_SESSION['MODERATEUR'] = $data[$i];
						$val = "ok";
						break;
						
					}else{
						$val="non";
					}
				}

				if ($val=="ok") {
					$_SESSION['MODERATEUR']['nom'] = $data['nom'];
					$_SESSION['MODERATEUR']['photo_profil'] = $data['photo_profil'];
					$_SESSION['MODERATEUR']['email'] = $_POST['email'];
					$_SESSION['MODERATEUR']['id_users'] = $moderateur[$i]['id_users'];
					$_SESSION['MODERATEUR']['id'] = $moderateur[$i]['id'];
					$_SESSION['MODERATEUR']['pwd'] = $moderateur[$i]['pwd'];
					redirect(site_url(array('Moderation', 'index')));
				} else {
					$_SESSION['ERR'] = 'Les paramtres recus ne correspondent a auccun adminstrateur dans notre Database.<br> <b>Veillez recommencer SVP</b>';
					redirect(site_url(array('Moderation', 'formulaireConnexion')));
				}
			} else {
				redirect(site_url(array('Moderation', 'formulaireConnexion')));
			}
		}


	// Fonction  qui liste tous les uttilisateurs
		public function ListUsers(){

			if (isset($_SESSION['MODERATEUR'])) {
				$alluse=$this->Users->findAllUsersSupBd();
				$allabo=$this->Users->findAboInfosBd();
				$allred=$this->Users->findRedInfosBd();
				for ($i=0; $i <$allabo['total'] ; $i++) { 
					$data['AllUsers'][$i]=$allabo[$i];
				}
				$k=0;
				for ($j=$allabo['total']; $j <$allabo['total']+$allred['total'] ; $j++) { 
					$data['AllUsers'][$j]=$allred[$k];

					$k++;
				}
				$k=0;
				for ($m=$allabo['total']+$allred['total']; $m <$allabo['total']+$allred['total']+$alluse['total'] ; $m++) { 
					$data['AllUsers'][$m]=$alluse[$k];

					$k++;
				}
				$data['AllUsers']['total']=$m;
				$this->load->view('MODERATEUR/index');
				$this->load->view('template_al/navigation_moderateur');
				$this->load->view('MODERATEUR/listusers',$data);
				$this->load->view('MODERATEUR/footer');
			} else {
				session_destroy();
				redirect(site_url(array('Moderation', 'formulaireConnexion')));
			}
		}

	// fonction qui affiche un user
		public function profileUser(){
			if (isset($_SESSION['MODERATEUR'])) {
				$data['id']=$_POST['id'];
				$data['nom']=$_POST['nom'];
				$data['niveau']=$_POST['niveau'];
				$data['email']=$_POST['email'];
				$data['photo_profil']=$_POST['photo_profil'];
				$this->load->view('MODERATEUR/index');
				$this->load->view('template_al/navigation_moderateur');
				$this->load->view('MODERATEUR/formulaire_renseignement', $data);
				$this->load->view('MODERATEUR/footer');
			} else {

				redirect(site_url(array('Moderation', 'index')));	
			}
		}

	//fonction qui affiche le formulaire de suppression des users

		public function AffFormSupUs(){
			if ($_SESSION['MODERATEUR']) {
				$data['id']=$_POST['id'];
				$data['nom']=$_POST['nom'];
				$data['niveau']=$_POST['niveau'];
				$data['email']=$_POST['email'];
				$data['photo_profil']=$_POST['photo_profil'];
				$this->load->view('MODERATEUR/index');
				$this->load->view('template_al/navigation_moderateur');
				$this->load->view('MODERATEUR/formulaire_suppression_use',$data);
				$this->load->view('MODERATEUR/footer');
			}else {
				session_destroy();
				redirect(site_url(array('Moderation', 'formulaireConnexion')));
			}
		}

	//fonction qui supprime un user sur la liste
		public function SupUsers(){
			if ($_SESSION['MODERATEUR']) {
				if (isset($_POST) AND !empty($_POST)) {
					// if ($_POST['niveau']==1) {
					// 	$this->Admin->suppAdm($_POST['id']);
					// }elseif($_POST['niveau']==2) {
					// 	$this->Moderateur->suppMod($_POST['id']);
					// }elseif ($_POST['niveau']==3) {
					// 	$this->Redacteur->suppRed($_POST['id']);
					// }elseif ($_POST['niveau']==4) {
					// 	$this->Abonne->suppAbo($_POST['id']);
					// }
					print_r($_POST);
					$data['id']=$_POST['id'];
					$data['nom']=$_POST['nom'];
					$data['niveau']=$_POST['niveau'];
					$data['email']=$_POST['email'];
					$data['photo_profil']=$_POST['photo_profil'];
					$this->Users->hydrate($data);
					$this->Users->suppUsers($_POST['id']);
					redirect(site_url(array('Moderation', 'ListUsers')));
				}else{echo "string";}
			}else {
				session_destroy();
				redirect(site_url(array('Moderation', 'formulaireConnexion')));
			}
		}

	// fonction qui affiche la liste des categories 

		public function ListCategorie(){

			if (isset($_SESSION['MODERATEUR'])) {

				$data['AllCategorie'] = $this->Categorie->findAllCategorieBd();
				// print_r($data);
				$this->load->view('MODERATEUR/index');
				$this->load->view('template_al/navigation_moderateur');
				$this->load->view('MODERATEUR/listcategorie',$data);
				$this->load->view('MODERATEUR/footer');
			} else {
				session_destroy();
				redirect(site_url(array('Moderation', 'formulaireConnexion')));
			}
		}
		
			public function AffCategorie(){
					if (isset($_SESSION['MODERATEUR'])) {
						if (isset($_POST)) {
							$data['AllCategorie']=$this->Categorie->findOneCategorie($_POST['id']);
							$this->load->view('MODERATEUR/index');
							$this->load->view('template_al/navigation');
							$this->load->view('MODERATEUR/aff_categorie',$data);
							$this->load->view('MODERATEUR/footer');
						}
						
					} else {

						 redirect(site_url(array('Moderation', 'index')));	
					}
		}

	// fonction qui permet d'ajouter une categorie 

		public function AddCategorie(){

			if (isset($_SESSION['MODERATEUR'])) {
				$data['AllCategorie']=$this->Categorie->findAllCategorieBd();
				$this->load->view('MODERATEUR/index');
				$this->load->view('template_al/navigation_moderateur');
				$this->load->view('MODERATEUR/add_categorie',$data);
				$this->load->view('MODERATEUR/footer');
			} else {
				session_destroy();
				redirect(site_url(array('Moderation', 'formulaireConnexion')));
			}
		}

		// ajout categorie en BD

		public function AddCategories(){
			if (isset($_SESSION['MODERATEUR'])) {
				if(isset($_POST) and isset($_FILES)){
						// print_r($_SESSION['ADMIN']);
					if (isset($_FILES['image']) and $_FILES['image']['error'] == 0) {
						// Testons si le fichier n'est pas trop gros
						if ($_FILES['image']['size'] <= 100000000) {
							// Testons si l'extension est autorisée
							$infosfichier = pathinfo($_FILES['image']['name']);
							$extension_upload = $infosfichier['extension'];

							$config = $_FILES['image']['name'] . date('d') . '-' . date('m') . '-' . date('Y') . 'a' . date('H') . '-' . date('i') . $_SESSION['ADMIN']['nom'];
							$ma_variable = str_replace('.', '_', $config);
							$ma_variable = str_replace(' ', '_', $config);
							$config = $ma_variable . '.' . $extension_upload;
							move_uploaded_file($_FILES['image']['tmp_name'], 'assets/images/' . $config);
							$data['image'] = $config;

						} else {
							$data['message_save'] = "L'image choisie  est endommagée  veuillez le remplacer svp !!";
							$data['message'] = 'non';

						}

						$data['nom'] = $_POST['nom'];
						$data['image'] = $config;
						// $data['nombre'] =$this->Article->findTotalArticleBd();
						// $data['nombre'] =55;
						$data['description'] = $_POST['description'];
						$data['date_time'] = date('Y-m-d H:i:s');
						$this->Categorie->hydrate($data);
						$this->Categorie->addCategories();
						redirect(site_url(array('Moderation', 'ListCategorie')));

					}
				} else {
					session_destroy();
					redirect(site_url(array('Moderation', 'formulaireConnexion')));
				}
			
			}
		}


		public function ModCategories(){
			if (isset($_SESSION['MODERATEUR'])) {
				if(isset($_POST) and isset($_FILES)){
						// print_r($_SESSION['ADMIN']);
					if (isset($_FILES['image']) and $_FILES['image']['error'] == 0) {
						// Testons si le fichier n'est pas trop gros
						if ($_FILES['image']['size'] <= 100000000) {
							// Testons si l'extension est autorisée
							$infosfichier = pathinfo($_FILES['image']['name']);
							$extension_upload = $infosfichier['extension'];

							$config = $_FILES['image']['name'] . date('d') . '-' . date('m') . '-' . date('Y') . 'a' . date('H') . '-' . date('i') . $_SESSION['ADMIN']['nom'];
							$ma_variable = str_replace('.', '_', $config);
							$ma_variable = str_replace(' ', '_', $config);
							$config = $ma_variable . '.' . $extension_upload;
							move_uploaded_file($_FILES['image']['tmp_name'], 'assets/images/' . $config);
							$data['image'] = $config;
						} else {
							$data['message_save'] = "La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
							$data['message'] = 'non';
						}
					} else {
						$data['message_save'] = "L'image choisie  est endommagée  veuillez le remplacer svp !!";
						$data['message'] = 'non';
					}	


						$data['nom'] = $_POST['nom'];
						//$data['image'] = $_POST['image'];
						$data['description'] = $_POST['description'];
						$data['id'] = $_POST['id'];

						$data['date_time'] = date('Y-m-d H:i:s');
						$this->Categorie->hydrate($data);
						$this->Categorie->modCategories($_POST['id']);
						redirect(site_url(array('Moderation', 'ListCategorie')));

				}
				
				} else {
				session_destroy();
				redirect(site_url(array('Moderation', 'formulaireConnexion')));
			}
		}


	//fonction qui supprime une catégorie sur la liste
		public function SupCategorie(){
			if ($_SESSION['MODERATEUR']) {
				if (isset($_POST) AND !empty($_POST)) {
					$this->Categorie->suppCategorie($_POST['id']);
					redirect(site_url(array('Moderation', 'ListCategorie')));
				}else{echo "string";}
			}else {
				session_destroy();
				redirect(site_url(array('Moderation', 'formulaireConnexion')));
			}
		}

	//fonction qui affiche le formulaire de suppression de categorie
		public function AffFormSupCat(){
			if ($_SESSION['MODERATEUR']) {
				$data['id']=$_POST['id'];
				$this->load->view('MODERATEUR/index');
				$this->load->view('template_al/navigation_moderateur');
				$this->load->view('MODERATEUR/formulaire_suppression_cat',$data);
				$this->load->view('MODERATEUR/footer');
			}else {
				session_destroy();
				redirect(site_url(array('Moderation', 'formulaireConnexion')));
			}
		}

		// consultation d'un article

		public function ListArticle(){
			if (isset($_SESSION['MODERATEUR'])) {
				$data['AllArticle'] = $this->Article->findAllArticleBd();
				$this->load->view('MODERATEUR/index');
				$this->load->view('template_al/navigation_moderateur');
				$this->load->view('MODERATEUR/listarticle',$data);						
				$this->load->view('MODERATEUR/footer');
			} else {
				session_destroy();
				redirect(site_url(array('Moderation', 'formulaireConnexion')));
			}
		}

		// if ($_POST['numpag']==1) {
		// 			$this->load->view('MODERATEUR/listarticle',$data);		
		// 			}
		// 		if ($_POST['numpag']==2) {
		// 			$this->load->view('MODERATEUR/listarticlepost',$data);		
		// 			}

	// fonction qui permet d'afficher un article posté

		public function ListArticlePos(){
			if (isset($_SESSION['MODERATEUR'])) {
				$data['AllArticle'] = $this->Article->findAllArticleBd();
				$this->load->view('MODERATEUR/index');
				$this->load->view('template_al/navigation_moderateur');
				$this->load->view('MODERATEUR/listarticlepost',$data);
				$this->load->view('MODERATEUR/footer');
			} else {
				session_destroy();
				redirect(site_url(array('Moderation', 'formulaireConnexion')));
			}
		}



	// fonction qui permet d'ajouter un article

		public function AddArticles(){

			if (isset($_SESSION['MODERATEUR'])) {
				$data['AllArticle']=$this->Article->findAllArticleBd();

				$this->load->view('MODERATEUR/index');
				$this->load->view('template_al/navigation_moderateur');
				$this->load->view('MODERATEUR/add_article',$data);
				$this->load->view('MODERATEUR/footer');
			} else {
				session_destroy();
				redirect(site_url(array('Moderation', 'formulaireConnexion')));
			}
		}


	// fonction qui permet d'afficher le profil dun article
	
		public function AffArticle(){
					if (isset($_SESSION['MODERATEUR'])) {
						if (isset($_POST)) {
							$data['AllArticle']=$this->Article->findOneArticle($_POST['id']);
							$this->load->view('MODERATEUR/index');
							$this->load->view('template_al/navigation_moderateur');
							$this->load->view('MODERATEUR/aff_article',$data);
							$this->load->view('MODERATEUR/footer');
						}
						
					} else {

						 redirect(site_url(array('Moderation', 'index')));	
					}
		}

	//fonction qui supprime un article sur la liste
		public function SupArticle(){
			if ($_SESSION['MODERATEUR']) {
				if (isset($_POST) AND !empty($_POST)) {
					$this->Article->suppArticle($_POST['id']);
					redirect(site_url(array('Moderation', 'ListArticle')));
				}else{echo "string";}
			}else {
				session_destroy();
				redirect(site_url(array('Moderation', 'formulaireConnexion')));
			}
		}


	//fonction qui affiche le formulaire de suppression des articles
		public function AffFormSupArt(){
			if ($_SESSION['MODERATEUR']) {
				$data['id']=$_POST['id'];
				$this->load->view('MODERATEUR/index');
				$this->load->view('template_al/navigation_moderateur');
				if ($_POST['numpag']==1) {
					$data['num']=$_POST['numpag'];
				}
				if ($_POST['numpag']==2) {
					$data['num']=$_POST['numpag'];		
				}
				$this->load->view('MODERATEUR/formulaire_suppression_art',$data);
				$this->load->view('MODERATEUR/footer');
			}else {
				session_destroy();
				redirect(site_url(array('Moderation', 'formulaireConnexion')));
			}
		}


	// fonction qui modifie un article

		public function ModArticles(){
			if (isset($_SESSION['MODERATEUR'])) {
				if(isset($_POST) and isset($_FILES)){
						// print_r($_SESSION['ADMIN']);
					if (isset($_FILES['image']) and $_FILES['image']['error'] == 0) {
						// Testons si le fichier n'est pas trop gros
						if ($_FILES['image']['size'] <= 100000000) {
							// Testons si l'extension est autorisée
							$infosfichier = pathinfo($_FILES['image']['name']);
							$extension_upload = $infosfichier['extension'];

							$config = $_FILES['image']['name'] . date('d') . '-' . date('m') . '-' . date('Y') . 'a' . date('H') . '-' . date('i') . $_SESSION['ADMIN']['nom'];
							$ma_variable = str_replace('.', '_', $config);
							$ma_variable = str_replace(' ', '_', $config);
							$config = $ma_variable . '.' . $extension_upload;
							move_uploaded_file($_FILES['image']['tmp_name'], 'assets/images/' . $config);
							$data['image'] = $config;
						} else {
							$data['message_save'] = "La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
							$data['message'] = 'non';
						}
					} else {
						$data['message_save'] = "L'image choisie  est endommagée  veuillez le remplacer svp !!";
						$data['message'] = 'non';
					}	


						// $data['id_categorie'] = $_POST['id_categorie'];
						// $data['id_redacteur'] = $_POST['id_redacteur'];
						$data['titre'] = $_POST['titre'];
						$data['image'] = $config;
						$data['contenu'] = $_POST['contenu'];
						$data['id'] = $_POST['id'];

						$data['date_time'] = date('Y-m-d H:i:s');
						$this->Article->hydrate($data);
						$this->Article->modArticles($_POST['id']);
						redirect(site_url(array('Moderation', 'ListArticle')));

				}
				
				} else {
				session_destroy();
				redirect(site_url(array('Moderation', 'formulaireConnexion')));
			}
		}

			//fonction qui liste les commentaires
		public function ListComment (){
			if ($_SESSION['MODERATEUR']) {
				$data['AllComment'] = $this->Commentaire->findAllCommentBd();
				$this->load->view('MODERATEUR/index');
				$this->load->view('template_al/navigation_moderateur');
				$this->load->view('MODERATEUR/listcomment',$data);
				$this->load->view('MODERATEUR/footer');
			} else {
				session_destroy();
				redirect(site_url(array('Moderation', 'formulaireConnexion')));
			}
		}


	//fonction qui supprime un commentaire sur la liste
		public function SupComment(){
			if ($_SESSION['MODERATEUR']) {
				if (isset($_POST) AND !empty($_POST)) {
					$this->Commentaire->suppComment($_POST['id']);
					redirect(site_url(array('Moderation', 'ListComment')));
				}else{echo "string";}
			}else {
				session_destroy();
				redirect(site_url(array('Moderation', 'formulaireConnexion')));
			}
		}

	//fonction qui affiche le formulaire de suppression de commentaire
		public function AffFormSupCom(){
			if ($_SESSION['MODERATEUR']) {
				$data['id']=$_POST['id'];
				$this->load->view('MODERATEUR/index');
				$this->load->view('template_al/navigation_moderateur');
				$this->load->view('MODERATEUR/formulaire_suppression_com',$data);
				$this->load->view('MODERATEUR/footer');
			}else {
				session_destroy();
				redirect(site_url(array('Moderation', 'formulaireConnexion')));
			}
		}

	//fonction qui affiche le profil d'un admin
		public function profile(){
			if (isset($_SESSION['MODERATEUR'])) {
				// print_r($_SESSION['MODERATEUR']);
				$this->load->view('MODERATEUR/index');
				$this->load->view('template_al/navigation_moderateur');
				$this->load->view('MODERATEUR/profile');
				$this->load->view('MODERATEUR/footer');
			} else {

				redirect(site_url(array('Moderation', 'index')));	
			}
		}


	//fonction qui deconnecte un moderateur
		public function deconnexion()
		{
			if (isset($_SESSION['MODERATEUR'])) {
				session_destroy();
			}
			redirect(site_url(array('Moderation', 'index')));
			
		}




}

?>

