<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Welcome extends CI_Controller {

		
		public function index()
		{	
			if (isset($_SESSION['ABONNE'])) {
				$data['article'] = $this->Article->findAllArticleBd();
				// print_r($data['article']);
				$data['categorie'] = $this->Categorie->findAllCategorieBd();
				$data['redacteur']=$this->Users->findAllUsers_redacBd();
				// print_r($data['redacteur']);
				$this->load->view('WELCOME/header');
				$this->load->view('WELCOME/index',$data);
				$this->load->view('WELCOME/footer');
			} else{
				session_destroy();
				$data['article'] = $this->Article->findAllArticleBd();
				// print_r($data['article']);
				$data['categorie'] = $this->Categorie->findAllCategorieBd();
				$data['redacteur']=$this->Users->findAllUsers_redacBd();
				// print_r($data['redacteur']);
				$this->load->view('WELCOME/header');
				$this->load->view('WELCOME/index',$data);
				$this->load->view('WELCOME/footer');
			 }	
		}

		// fonction qui charge le formulaire d'inscription pour un abonne
		public function inscription()
		{	

			

		
			$this->load->view('WELCOME/header','',false);
			$this->load->view('WELCOME/inscription');
			$this->load->view('WELCOME/footer');
		}




		public function formulaireConnexion_abonne()
	{	
		if (isset($_SESSION['ABONNE'])) {
			redirect(site_url(array('Welcome', 'index')));
		} else {
			session_destroy();
			$this->load->view('WELCOME/header','',false);
			$this->load->view('WELCOME/formulaire_connexion_abonne');
			$this->load->view('WELCOME/footer');
		}
		
	}


		



		//fonction qui permet a un ABONNE de se connecter a sa session
	public function Connexion_abonne()
	{

		if (isset($_POST['email']) && isset($_POST['pwd'])) {

			$abonne = $this->Abonne->findAllAboBd();
			for ($i = 0; $i < $abonne['total']; $i++) {
				if ($abonne[$i]['email'] == $_POST['email'] && $abonne[$i]['pwd'] == $_POST['pwd']) {
					$a = $abonne[$i]['id_users'];
					$b = $abonne[$i]['id'];
					$data = $this->Users->findUsersInfos($a);
					// print_r($abonne);
					// print_r($data);
					$_SESSION['ABONNE'] = $data;
					$val = "ok";
					break;
					
				}else{
					$val="non";
				}
			}

			if ($val=="ok") {
				$_SESSION['ABONNE']['nom'] = $data['nom'];
				$_SESSION['ABONNE']['photo_profil'] = $data['photo_profil'];
				$_SESSION['ABONNE']['email'] = $_POST['email'];  //  ICI .........
				$_SESSION['ABONNE']['pwd'] = $_POST['pwd']; //  ICI .............
				$_SESSION['ABONNE']['id'] = $b; //  ICI .............
				redirect(site_url(array('Welcome', 'index')));
			} else {
				$_SESSION['ERR'] = 'Les paramtres recus ne correspondent a auccun abonne dans notre Database.<br> <b>Veillez recommencer SVP</b>';
				redirect(site_url(array('Welcome', 'formulaireConnexion_abonne')));
			 }
		} else {
			redirect(site_url(array('Welcome', 'formulaireConnexion_abonne')));
		}
	
	}


		// fonction qui connecte un anonyme a sa session apres connexion
		public function session_abonne()
	    {
		
			
 			

			if (isset($_POST)) {

				
		 $this->form_validation->set_rules('nom', 'Nom' , 'required|max_length[20]' , array('required' => 'veuillez entrer le %s' , 'max_length' => 'votre %s est trop long veuillez changer cela!' ));
		 
		  $this->form_validation->set_rules('email', 'Email' , 'required|max_length[20]' , array('required' => 'veuillez entrer le %s' , 'max_length' => 'Votre %s est trop long veuillez changer cela!' ));
		 
		$this->form_validation->set_rules('pwd', 'Mot de passe' , 'required|min_length[5]' , array('required' => 'veuillez entrer le %s' , 'min_length' => 'Votre %s est trop cour veuillez changer cela!' ));
		 $this->form_validation->set_rules('cpwd', 'Confirmer Mot de passe' , 'required|matches[pwd]' ,array('required' => 'veuillez confirmer le %s' , 'matches' => 'Echec de confirmation de mot de passe' ));

		if($this->form_validation->run()){

			
				if (isset($_FILES['photo_profil']) and $_FILES['photo_profil']['error'] == 0) {
					// Testons si le fichier n'est pas trop gros
					if ($_FILES['photo_profil']['size'] <= 100000000) {
						// Testons si l'extension est autorisée
						$infosfichier = pathinfo($_FILES['photo_profil']['name']);
						$extension_upload = $infosfichier['extension'];

						$config = $_FILES['photo_profil']['name'] . date('d') . '-' . date('m') . '-' . date('Y') . 'a' . date('H') . '-' . date('i');
						$ma_variable = str_replace('.', '_', $config);
						$ma_variable = str_replace(' ', '_', $config);
						$config = $ma_variable . '.' . $extension_upload;
						move_uploaded_file($_FILES['photo_profil']['tmp_name'], 'assets/images/' . $config);
						$data['photo_profil'] = $config;
					} else {
						$data['message_save'] = "La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
						$data['message'] = 'non';
					}
				} else {
					$data['message_save'] = "L'image choisie  est endommagée  veuillez le remplacer svp !!";
					$data['message'] = 'non';
				}

				$data['nom'] = $_POST['nom'];
				$data['email'] = $_POST['email'];
				$data['niveau'] = $_POST['niveau'];

				$data['date'] = date('Y-m-d H:i:s');
				$this->Users->hydrate($data);
				$data['id_users']=$this->Users->addUser();
				print_r($data['id_users']);
				if ($_POST['niveau']==4) {
					$data['pwd']=$_POST['pwd'];
					$data['id_users']=$data['id_users'];
					$this->Abonne->hydrate($data);
					$this->Abonne->addAbonne();
				}
				$_SESSION['message_save'] = "Administrateurs enregistré avec success !!";
				$_SESSION['success'] = 'ok';
				$_SESSION['ABONNE']['nom']=$_POST['nom'];
				$_SESSION['ABONNE']['photo_profil']=$data['photo_profil'];

				$abonne = $this->Abonne->findEndAbo();
				print_r($abonne);
				$_SESSION['ABONNE']['id']=$abonne;


				$this->session->set_flashdata('message', 'INSCRIPTION REALISER AVEC SUCCES');
				redirect(site_url(array('Welcome', 'index')));
			
			}else {
					$this->session->set_flashdata('message', 'ECHEC D\'INSCRIPTION');

					

				$this->load->view('WELCOME/header','',false); 
				?> <center style="background-color: red; border-radius:4px;font-weight: bold"> <?php echo $this->session->message; ?> </center> <?php 
			$this->load->view('WELCOME/inscription');
			$this->load->view('WELCOME/footer');
				/*redirect(site_url(array('Welcome', 'inscription')));*/
			}
	    }
	}



	public function blog(){
		$data['categorie'] = $this->Categorie->findAllCategorieBd();
		$this->load->view('WELCOME/header');
		$this->load->view('WELCOME/blog',$data);
		$this->load->view('WELCOME/footer');
	}


	// fonction qui charge les article de chaque categorie
	public function article(){
		if(isset($_POST)){
			$id_categorie = $_POST['id'];
			// print_r($id_categorie);
			$data['article_categorie'] = $this->Article->findArticleCategorie($id_categorie);
			// print_r($data['article_categorie']);
			$this->load->view('WELCOME/header');
			$this->load->view('WELCOME/article',$data);
			$this->load->view('WELCOME/footer');    }
		
		
	}

	// fonction qui charge l'article et ces commentaires
	public function article_complet(){
		if (isset($_POST) OR isset($_SESSION['ABONNE'])) {
			// print_r($_POST);
			if ($_POST['statut'] == 1) {
				$id_article = $_POST['id'];
				// print_r($id_article);
				$data['article_categorie'] = $this->Article->findInfoAllArticleBd($id_article);
				$data['commentaire'] = $this->Commentaire->findAllCommentaire($id_article);
				// print_r($data['commentaire']);
				for ($i=0; $i <$data['commentaire']['total'] ; $i++) { 
					// print_r($data['commentaire'][$i]['id_abonne']);
					// $id_users=[];
					$id_users[$i] = $this->Abonne->findAboId_users($data['commentaire'][$i]['id_abonne']);

					$data['AbonneInfos'][$i] = $this->Users->findUsersInfos($id_users[$i][0]['id_users']);
					// print_r($data['AbonneInfos'][$i]);
				}
				// print_r($id_users);
				// echo "<br><br><br><br><br><br>";
				// print_r($data['AbonneInfos']);

		}elseif ($_POST['statut'] == 2) {
			if (isset($_SESSION['ABONNE'])) {
				
				// print_r($_POST);
				$donnees['id_article'] = $_POST['id_article'];
				$donnees['id_abonne'] = $_POST['id_abonne'];
				$donnees['contenu'] = $_POST['contenu'];
				$donnees['date_time'] = date('Y-m-d H:i:s');
				$this->Commentaire->hydrate($donnees);
				$this->Commentaire->addComment();
				$_SESSION['id_article'] = $donnees['id_article'];
				// $_SESSION['ABONNE']['id'] = $donnees['id_abonne'];


				$data['article_categorie'] = $this->Article->findInfoAllArticleBd($donnees['id_article']);
				$data['commentaire'] = $this->Commentaire->findAllCommentaire($donnees['id_article']);
				for ($i=0; $i <$data['commentaire']['total'] ; $i++) { 
				$id_users[$i] = $this->Abonne->findAboId_users($data['commentaire'][$i]['id_abonne']);
				$data['AbonneInfos'][$i] = $this->Users->findUsersInfos($id_users[$i][0]['id_users']);
				// $data['AbonneInfos'][$i] = $this->Users->findUsersInfos($id_users[$i]);
				}


			}else{
				redirect(site_url(array('Welcome','formulaireConnexion_abonne')));

			}
		}


			$this->load->view('WELCOME/header');
			$this->load->view('WELCOME/article_complet',$data);
			$this->load->view('WELCOME/footer');
			
		}
	}
	



	public function deconnexion()
	{
		if (isset($_SESSION['ABONNE'])) {
			session_destroy();
		
		}	
		print_r($_SESSION);
		redirect(site_url(array('Welcome', 'index')));
	}

		
}
