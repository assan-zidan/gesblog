<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Redaction extends CI_Controller {

			
	public function index(){
		
		 if (isset($_SESSION['REDACTEUR'])) {
			$this->load->view('REDACTEUR/index');
			$this->load->view('template_al/navigation_r');
			$this->load->view('REDACTEUR/home');
			$this->load->view('REDACTEUR/footer');
		
				
		}else{
	   	 session_destroy();
		 redirect(site_url(array('Redaction','formulaireConnexion')));
		}
	}

	// fonction qui charge le formulaire de connexion pour un rédacteur

		public function formulaireConnexion(){
			
			if (isset($_SESSION['REDACTEUR'])) {
				if (isset($_SESSION['REDACTEUR'])) {
					redirect(site_url(array('Redaction','index')));
				}else{
					session_destroy();
					redirect(site_url(array('Redaction','formulaireConnexion')));
				}
			}else{
				$this->load->view('gestion_redacteur/formulaire_connexion');
			}
		}

		public function manageConnexion(){
			
			if (isset($_POST['email']) && isset($_POST['pwd'])) {
				
				$red = $this->Redacteur->findAllRedBd();
				for ($i=0; $i < $red['total']; $i++) { 
					if ($red[$i]['email'] == $_POST['email'] && $red[$i]['pwd'] == $_POST['pwd']) {
						$z=$red[$i]['id_users'];
						$r=$this->Users->findRedInfos($z);
						$_SESSION['REDACTEUR'] = $red[$i];
						$_SESSION['REDACTEUR']['nom'] = $r['nom'];
						$_SESSION['REDACTEUR']['photo_profil'] = $r['photo_profil'];
					}
				}
				
				if (isset($_SESSION['REDACTEUR'])) {
					redirect(site_url(array('Redaction','index')));
				}else{
					$_SESSION['ERR'] = 'Les parametres recus ne correspondent a aucun rédacteur dans notre Database.<br> <b>Veuillez recommencer SVP</b>';
					redirect(site_url(array('Redaction','formulaireConnexion')));
				}
			}else{
				redirect(site_url(array('Redaction','formulaireConnexion')));
			}
		}

	// Gestions des rédacteurs


		public function manageRed(){
			
			if (isset($_SESSION['REDACTEUR'])) {
				
				$data['AllRed']=$this->Redacteur->findAllRedBd();
				$this->load->view('WELCOME/index',$data);
				$this->load->view('template_al/navigation');
				$this->load->view('REDACTEUR/home');
				$this->load->view('WELCOME/footer');
				
			}else{
				session_destroy();
				redirect(site_url(array('Redaction','formulaireConnexion')));
			}
		}

		public function testExitRed($email){
	        $etat=0;
	        $data['infoRed']=$this->Redacteur->findAllRedBd();
	        if ($data['infoRed']['total']<=0) {
	            
	        }else{
	            for ($i=0; $i <$data['infoRed']['total'] ; $i++) { 
	                if ($data['infoRed'][$i]['email']==$email) {
	                    $etat=1;
	                    break;
	                }else{
	                    $etat=0;
	                }
	            }
	        }
	        return $etat;
		}

	// Ajout d'un redacteur

		public function addRed(){
			if (isset($_SESSION['REDACTEUR'])) {
				if (isset($_POST)) {
					$etat=$this->testExitRed($_POST['email']);
					if ($etat==0) {
						if (isset($_FILES['profil']) AND $_FILES['profil']['error'] == 0 ){
		        		// Testons si le fichier n'est pas trop gros
		                    if ($_FILES['profil']['size'] <= 100000000){
		                        // Testons si l'extension est autorisée
		                        $infosfichier =pathinfo($_FILES['profil']['name']);
		                        $extension_upload = $infosfichier['extension'];

		                        $config =$_FILES['profil']['name'].date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i').$_SESSION['REDACTEUR']['id'];
		 						$ma_variable = str_replace('.', '_', $config);
								$ma_variable = str_replace(' ', '_', $config);
								$config = $ma_variable.'.'.$extension_upload;
								move_uploaded_file($_FILES['profil']['tmp_name'],'assets/images/user_profil/'.$config);
								$data['profil']=$config;
								
		                    }else{
		                        $data['message_save']="La taille du fichier choisi  est très grande veuillez le remplacer svp !!";
		                        $data['message']='non';
		                    }
		                }else{
		                    $data['message_save']="L'image choisie  est endommagée  veuillez la remplacer svp !!";
		                    $data['message']='non';
		                }
						
						$data['nom']=$_POST['nom'];
						$data['email']=$_POST['email'];
						$data['password']=$_POST['password'];
						$data['telephone']=$_POST['telephone'];
						
						$data['date']=date('Y-m-d H:i:s');
						$this->Redacteur->hydrate($data);
						$this->Redacteur->addRed();
						$_SESSION['message_save']="Rédacteur enregistré avec success !!";
				 		$_SESSION['success']='ok';
				 		redirect(site_url(array('Redaction','manageRed')));
						
					}else{
						session_destroy();
						direct(site_url(array('Redaction','formulaireConnexion')));
					}
				}else{
					session_destroy();
					redirect(site_url(array('Redaction','formulaireConnexion')));
				}
			}else{
				session_destroy();
				redirect(site_url(array('Redaction','formulaireConnexion')));
			}

		}

	// consultation d'un article

		public function ListArticle(){

			if ($_SESSION['REDACTEUR']) {
				$data['AllArticle'] = $this->Article->findAllArticleBd();

				$this->load->view('REDACTEUR/index');
				$this->load->view('template_al/navigation_r');
				$this->load->view('REDACTEUR/listarticle',$data);
				$this->load->view('REDACTEUR/footer');
			} else {
				session_destroy();
				redirect(site_url(array('Redaction', 'formulaireConnexion')));
				}
		}


	// Ajout d'un article

		public function AddArticle(){

			if (isset($_SESSION['REDACTEUR'])) {
				$donnees=$this->Categorie->findAllCategorieBd();
		       	for($i=0; $i<$donnees['total']; $i++) {
		       		$data['info'][$i]['id_categorie']= $donnees[$i]['id'];
			   		$data['info'][$i]['nom']=$donnees[$i]['nom'];
			       	$data['info']['total']=$donnees['total'];
			    }
				$data['AllArticle']=$this->Article->findAllArticleBd();
				$this->load->view('REDACTEUR/index');
				$this->load->view('template_al/navigation_r');
				$this->load->view('REDACTEUR/add_article',$data);
				$this->load->view('REDACTEUR/footer');
			} else {
			session_destroy();
			redirect(site_url(array('Redaction', 'formulaireConnexion')));
			}
		}

	
	// Ajout articles en BD

		public function AddArticles(){
			if (isset($_SESSION['REDACTEUR'])) {
				if(isset($_POST) and isset($_FILES)){
						// print_r($_SESSION['REDACTEUR']);

					if (isset($_FILES['image']) and $_FILES['image']['error'] == 0) {
						// Testons si le fichier n'est pas trop gros
						if ($_FILES['image']['size'] <= 100000000) {
							// Testons si l'extension est autorisée
							$infosfichier = pathinfo($_FILES['image']['name']);
							$extension_upload = $infosfichier['extension'];

							$config = $_FILES['image']['name'] . date('d') . '-' . date('m') . '-' . date('Y') . 'a' . date('H') . '-' . date('i') . $_SESSION['REDACTEUR']['nom'];
							$ma_variable = str_replace('.', '_', $config);
							$ma_variable = str_replace(' ', '_', $config);
							$config = $ma_variable . '.' . $extension_upload;
							move_uploaded_file($_FILES['image']['tmp_name'], 'assets/images/' . $config);
							$data['image'] = $config;
						} else {
								$data['message_save'] = "La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
								$data['message'] = 'non';
							}
					} else {
							$data['message_save'] = "L'image choisie  est endommagée  veuillez le remplacer svp !!";
							$data['message'] = 'non';
						}		
					$data['titre'] = $_POST['titre'];
					$data['contenu'] = $_POST['contenu'];
					$data['etat'] = $_POST['etat'];
					$data['nb_like'] = "5";
					$data['date_time'] = date('Y-m-d H:i:s');
					$data['id_redacteur'] = $_SESSION['REDACTEUR']['id'];
					$data ['id_categorie'] = $_POST['categorie'];

			    	$this->Article->hydrate($data);
					$this->Article->addArticle();
					$data['AllArticle'] =$this->Article->findArticleBd($_SESSION['REDACTEUR']['id']);
					$this->load->view('REDACTEUR/index');
					$this->load->view('template_al/navigation_r');
					$this->load->view('REDACTEUR/listarticle',$data);
					$this->load->view('REDACTEUR/footer');
					
				}	

			} else {
					session_destroy();
					redirect(site_url(array('Redaction', 'formulaireConnexion')));
				}
		}





	public function ModArticles(){
		// print_r($_POST);
		// print_r($_FILES);
			if (isset($_SESSION['REDACTEUR'])) {
				if(isset($_POST) and isset($_FILES)){
						// print_r($_SESSION['REDACTEUR']);
					if (isset($_FILES['image']) and $_FILES['image']['error'] == 0) {
						// Testons si le fichier n'est pas trop gros
						if ($_FILES['image']['size'] <= 100000000) {
							// Testons si l'extension est autorisée
							$infosfichier = pathinfo($_FILES['image']['name']);
							$extension_upload = $infosfichier['extension'];

							$config = $_FILES['image']['name'] . date('d') . '-' . date('m') . '-' . date('Y') . 'a' . date('H') . '-' . date('i') . $_SESSION['REDACTEUR']['nom'];
							$ma_variable = str_replace('.', '_', $config);
							$ma_variable = str_replace(' ', '_', $config);
							$config = $ma_variable . '.' . $extension_upload;
							move_uploaded_file($_FILES['image']['tmp_name'], 'assets/images/' . $config);
							$data['image'] = $config;
						} else {
								$data['message_save'] = "La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
								$data['message'] = 'non';
							}
					} else {
							$data['message_save'] = "L'image choisie  est endommagée  veuillez le remplacer svp !!";
							$data['message'] = 'non';
						}

					$data['titre'] = $_POST['titre'];
					$data['contenu'] = $_POST['contenu'];
					$data['etat'] = $_POST['etat'];
					$data['nb_like'] = "5";
					$data['date_time'] = date('Y-m-d H:i:s');
					$data['id_redacteur'] = $_SESSION['REDACTEUR']['id'];
					$data ['id_categorie'] = $_POST['categorie'];
					$data ['target'] = $_POST['class'];

			    	$this->Article->hydrate($data);
					// $this->Article->modArticles($data['id_redacteur']);
					$this->Article->modArticles($data ['target']);
					$data['AllArticle'] =$this->Article->findArticleBd($_SESSION['REDACTEUR']['id']);
					$this->load->view('REDACTEUR/index');
					$this->load->view('template_al/navigation_r');
					$this->load->view('REDACTEUR/listarticle',$data);
					$this->load->view('REDACTEUR/footer');
					
				}


			} else {
					session_destroy();
					redirect(site_url(array('Redaction', 'formulaireConnexion')));
				}
		}


	// fonction qui permet dafficher le profil dun article



	
	public function AffArticle(){
				if (isset($_SESSION['REDACTEUR'])) {
					if (isset($_POST)) {
						// print_r($_POST);
						// print_r($_POST['nom']);
						$data['AllArticle']=$this->Article->findOneArticle($_POST['id']);
						// print_r($data['AllArticle']);
						$this->load->view('ADMIN/index');
						$this->load->view('template_al/navigation_r');
						$this->load->view('ADMIN/aff_article',$data);
						$this->load->view('ADMIN/footer');
					}
					
				} else {

					 redirect(site_url(array('Redaction', 'index')));	
				}
				
		
			
		}

	// ajout categorie en BD

		// public function AddCategories(){
		// 	if (isset($_SESSION['REDACTEUR'])) {
		// 		if(isset($_POST) and isset($_FILES)){
		// 				print_r($_SESSION['REDACTEUR']);
		// 			if (isset($_FILES['image']) and $_FILES['image']['error'] == 0) {
		// 				// Testons si le fichier n'est pas trop gros
		// 				if ($_FILES['image']['size'] <= 100000000) {
		// 					// Testons si l'extension est autorisée
		// 					$infosfichier = pathinfo($_FILES['image']['name']);
		// 					$extension_upload = $infosfichier['extension'];

		// 					$config = $_FILES['image']['name'] . date('d') . '-' . date('m') . '-' . date('Y') . 'a' . date('H') . '-' . date('i') . $_SESSION['REDACTEUR']['nom'];
		// 					$ma_variable = str_replace('.', '_', $config);
		// 					$ma_variable = str_replace(' ', '_', $config);
		// 					$config = $ma_variable . '.' . $extension_upload;
		// 					move_uploaded_file($_FILES['image']['tmp_name'], 'assets/images/' . $config);
		// 					$data['image'] = $config;
		// 				} else {
		// 					$data['message_save'] = "La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
		// 					$data['message'] = 'non';
		// 				}
		// 			} else {
		// 				$data['message_save'] = "L'image choisie  est endommagée  veuillez le remplacer svp !!";
		// 				$data['message'] = 'non';
		// 			}		
		// 				$data['nom'] = $_POST['nom'];
		// 				$data['image'] = $config;
		// 				$data['nombre'] =$this->Article->findTotalArticleBd();
		// 				print_r($data['nombre']);
		// 				$data['description'] = $_POST['description'];

		// 				$data['date_time'] = date('Y-m-d H:i:s');
		// 				$this->Article->hydrate($data);
		// 				$this->Article->addCategories();
		// 				redirect(site_url(array('Redaction', 'ListArticle')));

		// 		}
				
		// 		} else {
		// 		session_destroy();
		// 		redirect(site_url(array('Redaction', 'formulaireConnexion')));
		// 	}
		
		// }

	//déconnexion d'un redacteur

		public function deconnexion(){
			if (isset($_SESSION['REDACTEUR'])) {
				session_destroy();
			}
			redirect(site_url(array('Redaction','index')));
		}
}