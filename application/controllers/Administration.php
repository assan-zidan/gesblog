<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Administration extends CI_Controller{

		public function index(){

			if (isset($_SESSION['ADMIN'])) {
				$data['niveau']=$this->Users->findniveaUsersBd();
				$this->load->view('ADMIN/index');
				$this->load->view('template_al/navigation',$data);
				$this->load->view('ADMIN/home');
				$this->load->view('ADMIN/footer');
			} else {
				session_destroy();
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		}


	// fonction qui charge le formulaire de connexion pour un administrateur
		public function formulaireConnexion(){

			if (isset($_SESSION['ADMIN'])) {
				if (isset($_SESSION['ADMIN'])) {
					redirect(site_url(array('Administration', 'index')));
				} else {
					session_destroy();
					redirect(site_url(array('Administration', 'formulaireConnexion')));
				}
			} else {
				$this->load->view('gestion_admin/formulaire_connexion');
			}
		}

	//fonction qui permet a un administrateur de se connecter a sa session

		public function manageConnexion(){

			if (isset($_POST['email']) && isset($_POST['pwd'])) {

				$admin = $this->Admin->findAllAdminBd();
				for ($i = 0; $i < $admin['total']; $i++) {
					if ($admin[$i]['email'] == $_POST['email'] && $admin[$i]['pwd'] == $_POST['pwd']) {
						$a = $admin[$i]['id_users'];
						$data = $this->Users->findUsersInfos($a);
						// print_r($data);
						$_SESSION['ADMIN'] = $data[$i];
						$val = "ok";
						break;
						
					}else{
						$val="non";
					}
// =======
// 	public function manageConnexion()
// 	{

// 		if (isset($_POST['email']) && isset($_POST['pwd'])) {

// 			$admin = $this->Admin->findAllAdminBd();
// 			for ($i = 0; $i < $admin['total']; $i++) {
// 				if ($admin[$i]['email'] == $_POST['email'] && $admin[$i]['pwd'] == $_POST['pwd']) {
// 					$a = $admin[$i]['id_users'];
// 					$data = $this->Users->findUsersInfos($a);
// 					// print_r($data);
// 					$_SESSION['ADMIN'] = $data[$i];
// 					$val = "ok";
// 					break;
					
// 				}else{
// 					$val="non";
// >>>>>>> jael
				}

				if ($val=="ok") {
					$_SESSION['ADMIN']['nom'] = $data['nom'];
					$_SESSION['ADMIN']['photo_profil'] = $data['photo_profil'];
					$_SESSION['ADMIN']['email'] = $_POST['email'];  
					$_SESSION['ADMIN']['pwd'] = $_POST['pwd']; 
					$_SESSION['ADMIN']['id_users'] = $admin[$i]['id_users'];
					$_SESSION['ADMIN']['id'] = $admin[$i]['id'];
					redirect(site_url(array('Administration', 'index')));
				} else {
					$_SESSION['ERR'] = 'Les paramètres reçus ne correspondent a aucun administrateur dans notre Database.<br> <b>Veuillez recommencer SVP</b>';
					redirect(site_url(array('Administration', 'formulaireConnexion')));
				}
			} else {
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		}


	// Fonction  qui liste tous les uttilisateurs
		public function ListUsers(){

			if (isset($_SESSION['ADMIN'])) {

				$data['AllUsers'] = $this->Users->findAllUsersBd();

				$data['niveau']=$this->Users->findniveaUsersBd();
				$this->load->view('ADMIN/index');
				$this->load->view('template_al/navigation');
				$this->load->view('ADMIN/listusers',$data);
				$this->load->view('ADMIN/footer');
			} else {
				session_destroy();
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		}

	//fonction qui supprime un user sur la liste
		public function SupUsers(){
			if ($_SESSION['ADMIN']) {
				if (isset($_POST) AND !empty($_POST)) {
					// if ($_POST['niveau']==1) {
					// 	$this->Admin->suppAdm($_POST['id']);
					// }elseif($_POST['niveau']==2) {
					// 	$this->Moderateur->suppMod($_POST['id']);
					// }elseif ($_POST['niveau']==3) {
					// 	$this->Redacteur->suppRed($_POST['id']);
					// }elseif ($_POST['niveau']==4) {
					// 	$this->Abonne->suppAbo($_POST['id']);
					// }
					print_r($_POST);
					$data['id']=$_POST['id'];
					$data['nom']=$_POST['nom'];
					$data['niveau']=$_POST['niveau'];
					$data['email']=$_POST['email'];
					$data['photo_profil']=$_POST['photo_profil'];
					$this->Users->hydrate($data);
					$this->Users->suppUsers($_POST['id']);
					redirect(site_url(array('Administration', 'ListUsers')));
				}else{echo "string";}
			}else {
				session_destroy();
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		}

	//fonction qui affiche le formulaire de suppression des users

			public function AffFormSupUs(){
			if ($_SESSION['ADMIN']) {
				$data['id']=$_POST['id'];
				$data['nom']=$_POST['nom'];
				$data['niveau']=$_POST['niveau'];
				$data['email']=$_POST['email'];
				$data['photo_profil']=$_POST['photo_profil'];
				$this->load->view('ADMIN/index');
				$this->load->view('template_al/navigation');
				$this->load->view('ADMIN/formulaire_suppression_use',$data);
				$this->load->view('ADMIN/footer');
			}else {
				session_destroy();
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		}
		
		public function testExitAdmin($email){
	        $etat=0;
	        $data['infoAdmin']=$this->Admin->findAllAdminBd();
	        if ($data['infoAdmin']['total']<=0) {
	            
	        }else{
	            for ($i=0; $i <$data['infoAdmin']['total'] ; $i++) { 
	                if ($data['infoAdmin'][$i]['email']==$email) {
	                    $etat=1;
	                    break;
	                }else{
	                    $etat=0;
	                }
	            }
	        }
	        return $etat;
	    }   


	// fonction qui affiche la liste des categories (jael)

		public function ListCategorie(){

			if (isset($_SESSION['ADMIN'])) {

				$data['AllCategorie'] = $this->Categorie->findAllCategorieBd();
				$this->load->view('ADMIN/index');
				$this->load->view('template_al/navigation');
				$this->load->view('ADMIN/listcategorie',$data);
				$this->load->view('ADMIN/footer');
			} else {
				session_destroy();
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		}

	//fonction qui supprime une catégorie sur la liste
		public function SupCategorie(){
			if ($_SESSION['ADMIN']) {
				if (isset($_POST) AND !empty($_POST)) {
					$this->Categorie->suppCategorie($_POST['id']);
					redirect(site_url(array('Administration', 'ListCategorie')));
				}else{echo "string";}
			}else {
				session_destroy();
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		}

	//fonction qui affiche le formulaire de suppression de categorie
		public function AffFormSupCat(){
			if ($_SESSION['ADMIN']) {
				$data['id']=$_POST['id'];
				$this->load->view('ADMIN/index');
				$this->load->view('template_al/navigation');
				$this->load->view('ADMIN/formulaire_suppression_cat',$data);
				$this->load->view('ADMIN/footer');
			}else {
				session_destroy();
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		}


	// fonction qui permet d'ajouter une categorie 

		public function AddCategorie(){

			if (isset($_SESSION['ADMIN'])) {
				$data['AllCategorie']=$this->Categorie->findAllCategorieBd();
				$this->load->view('ADMIN/index');
				$this->load->view('template_al/navigation');
				$this->load->view('ADMIN/add_categorie',$data);
				$this->load->view('ADMIN/footer');
			} else {
				session_destroy();
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		}

	// ajout categorie en BD

		public function AddCategories(){
			if (isset($_SESSION['ADMIN'])) {
				if(isset($_POST) and isset($_FILES)){
						// print_r($_SESSION['ADMIN']);
					if (isset($_FILES['image']) and $_FILES['image']['error'] == 0) {
						// Testons si le fichier n'est pas trop gros
						if ($_FILES['image']['size'] <= 100000000) {
							// Testons si l'extension est autorisée
							$infosfichier = pathinfo($_FILES['image']['name']);
							$extension_upload = $infosfichier['extension'];

							$config = $_FILES['image']['name'] . date('d') . '-' . date('m') . '-' . date('Y') . 'a' . date('H') . '-' . date('i') . $_SESSION['ADMIN']['nom'];
							$ma_variable = str_replace('.', '_', $config);
							$ma_variable = str_replace(' ', '_', $config);
							$config = $ma_variable . '.' . $extension_upload;
							move_uploaded_file($_FILES['image']['tmp_name'], 'assets/images/' . $config);
							$data['image'] = $config;

						} else {
							$data['message_save'] = "L'image choisie  est endommagée  veuillez le remplacer svp !!";
							$data['message'] = 'non';

						}

// <<<<<<< HEAD
						$data['nom'] = $_POST['nom'];
						$data['image'] = $config;
						// $data['nombre'] =$this->Article->findTotalArticleBd();
// =======
// 					$data['nom'] = $_POST['nom'];
// 					$data['image'] = $config;
// 					// $data['nombre'] =$this->Article->findTotalArticleBd();
// 					// print_r($data['nombre']);
// 					$data['description'] = $_POST['description'];

// 					$data['date_time'] = date('Y-m-d H:i:s');
// 					$this->Categorie->hydrate($data);
// 					$this->Categorie->addCategories();
// 					redirect(site_url(array('Administration', 'ListCategorie')));
// >>>>>>> jael

						// print_r($data['nombre']);

// 						$data['nombre'] =55;

						$data['description'] = $_POST['description'];
						$data['date_time'] = date('Y-m-d H:i:s');
						$this->Categorie->hydrate($data);
						$this->Categorie->addCategories();
						redirect(site_url(array('Administration', 'ListCategorie')));

					}
				} else {
					session_destroy();
					redirect(site_url(array('Administration', 'formulaireConnexion')));
				}
			
			}
		}

		public function AffCategorie(){
					if (isset($_SESSION['ADMIN'])) {
						if (isset($_POST)) {
							// print_r($_POST);
							// print_r($_POST['nom']);
							$data['AllCategorie']=$this->Categorie->findOneCategorie($_POST['id']);
								// print_r($data['AllCategorie']);
							$this->load->view('ADMIN/index');
							$this->load->view('template_al/navigation');
							$this->load->view('ADMIN/aff_categorie',$data);
							$this->load->view('ADMIN/footer');
						}
						
					} else {

						 redirect(site_url(array('Administration', 'index')));	
					}
		}


		public function ModCategories(){
			if (isset($_SESSION['ADMIN'])) {
				if(isset($_POST) and isset($_FILES)){
						// print_r($_SESSION['ADMIN']);
					if (isset($_FILES['image']) and $_FILES['image']['error'] == 0) {
						// Testons si le fichier n'est pas trop gros
						if ($_FILES['image']['size'] <= 100000000) {
							// Testons si l'extension est autorisée
							$infosfichier = pathinfo($_FILES['image']['name']);
							$extension_upload = $infosfichier['extension'];

							$config = $_FILES['image']['name'] . date('d') . '-' . date('m') . '-' . date('Y') . 'a' . date('H') . '-' . date('i') . $_SESSION['ADMIN']['nom'];
							$ma_variable = str_replace('.', '_', $config);
							$ma_variable = str_replace(' ', '_', $config);
							$config = $ma_variable . '.' . $extension_upload;
							move_uploaded_file($_FILES['image']['tmp_name'], 'assets/images/' . $config);
							$data['image'] = $config;
						} else {
							$data['message_save'] = "La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
							$data['message'] = 'non';
						}
					} else {
						$data['message_save'] = "L'image choisie  est endommagée  veuillez le remplacer svp !!";
						$data['message'] = 'non';
					}	


						$data['nom'] = $_POST['nom'];
						$data['image'] = $config;
						$data['description'] = $_POST['description'];
						$data['id'] = $_POST['id'];

						$data['date_time'] = date('Y-m-d H:i:s');
						$this->Categorie->hydrate($data);
						$this->Categorie->modCategories($_POST['id']);
						redirect(site_url(array('Administration', 'ListCategorie')));

				}
				
				} else {
				session_destroy();
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		}

	// consultation d'un article

		public function ListArticle(){
			if (isset($_SESSION['ADMIN'])) {
				$data['AllArticle'] = $this->Article->findAllArticleBd();
				$this->load->view('ADMIN/index');
				$this->load->view('template_al/navigation');
				$this->load->view('ADMIN/listarticle',$data);
				$this->load->view('ADMIN/footer');
			} else {
				session_destroy();
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		}

	// fonction qui permet d'afficher un article posté

		public function ListArticlePos(){
			if (isset($_SESSION['ADMIN'])) {
				$data['AllArticle'] = $this->Article->findAllArticleBd();
				$this->load->view('ADMIN/index');
				$this->load->view('template_al/navigation');
				$this->load->view('ADMIN/listarticlepost',$data);
				$this->load->view('ADMIN/footer');
			} else {
				session_destroy();
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		}



	// fonction qui permet d'ajouter un article

		public function AddArticles(){


			if (isset($_SESSION['ADMIN'])) {
				$data['AllArticle']=$this->Article->findAllArticleBd();
				$this->load->view('ADMIN/index');
				$this->load->view('template_al/navigation');
				$this->load->view('ADMIN/add_article',$data);
				$this->load->view('ADMIN/footer');
			} else {
				session_destroy();
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		}


	// fonction qui permet d'afficher le profil dun article
	
// <<<<<<< HEAD
// 		public function AffArticle(){
// 					if (isset($_SESSION['ADMIN'])) {
// 						if (isset($_POST)) {
// 							$data['AllArticle']=$this->Article->findOneArticle($_POST['id']);
// 							$this->load->view('ADMIN/index');
// 							$this->load->view('template_al/navigation');
// 							$this->load->view('ADMIN/aff_article',$data);
// 							$this->load->view('ADMIN/footer');
// 						}
						
// 					} else {

// 						 redirect(site_url(array('Administration', 'index')));	
		// }
// =======
	public function VoirArticle(){
				if (isset($_SESSION['ADMIN'])) {
					if (isset($_POST)) {
						// print_r($_POST);
						// print_r($_POST['nom']);
						$data['AllArticle']=$this->Article->findOneArticle($_POST['id']);
							// print_r($data['AllCategorie']);
						$this->load->view('ADMIN/index');
						$this->load->view('template_al/navigation');
						$this->load->view('ADMIN/voir_article',$data);
						$this->load->view('ADMIN/footer');
// >>>>>>> jael
					}
		}

	}


	//fonction qui supprime un article sur la liste
		public function SupArticle(){
			if ($_SESSION['ADMIN']) {
				if (isset($_POST) AND !empty($_POST)) {
					$this->Article->suppArticle($_POST['id']);
					redirect(site_url(array('Administration', 'ListArticle')));
				}else{echo "string";}
			}else {
				session_destroy();
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		}


	//fonction qui affiche le formulaire de suppression des articles
		public function AffFormSupArt(){
			if ($_SESSION['ADMIN']) {
				$data['id']=$_POST['id'];
				$this->load->view('ADMIN/index');
				$this->load->view('template_al/navigation');
				$this->load->view('ADMIN/formulaire_suppression_art',$data);
				$this->load->view('ADMIN/footer');
			}else {
				session_destroy();
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		}

	// fonction qui modifie un article

		public function ModArticles(){
			if (isset($_SESSION['ADMIN'])) {
				if(isset($_POST) and isset($_FILES)){
						// print_r($_SESSION['ADMIN']);
					if (isset($_FILES['image']) and $_FILES['image']['error'] == 0) {
						// Testons si le fichier n'est pas trop gros
						if ($_FILES['image']['size'] <= 100000000) {
							// Testons si l'extension est autorisée
							$infosfichier = pathinfo($_FILES['image']['name']);
							$extension_upload = $infosfichier['extension'];

							$config = $_FILES['image']['name'] . date('d') . '-' . date('m') . '-' . date('Y') . 'a' . date('H') . '-' . date('i') . $_SESSION['ADMIN']['nom'];
							$ma_variable = str_replace('.', '_', $config);
							$ma_variable = str_replace(' ', '_', $config);
							$config = $ma_variable . '.' . $extension_upload;
							move_uploaded_file($_FILES['image']['tmp_name'], 'assets/images/' . $config);
							$data['image'] = $config;
						} else {
							$data['message_save'] = "La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
							$data['message'] = 'non';
						}
					} else {
						$data['message_save'] = "L'image choisie  est endommagée  veuillez le remplacer svp !!";
						$data['message'] = 'non';
					}	


						// $data['id_categorie'] = $_POST['id_categorie'];
						// $data['id_redacteur'] = $_POST['id_redacteur'];
						$data['titre'] = $_POST['titre'];
						$data['image'] = $config;
						$data['contenu'] = $_POST['contenu'];
						$data['id'] = $_POST['id'];

						$data['date_time'] = date('Y-m-d H:i:s');
						$this->Article->hydrate($data);
						$this->Article->modArticles($_POST['id']);
						redirect(site_url(array('Administration', 'ListArticle')));

				}
				
				} else {
				session_destroy();
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		}

	//fonction qui liste les commentaires
		public function ListComment (){
			if ($_SESSION['ADMIN']) {
				$data['AllComment'] = $this->Commentaire->findAllCommentBd();
				$this->load->view('ADMIN/index');
				$this->load->view('template_al/navigation');
				$this->load->view('ADMIN/listcomment',$data);
				$this->load->view('ADMIN/footer');
			} else {
				session_destroy();
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		}


	//fonction qui supprime un commentaire sur la liste
		public function SupComment(){
			if ($_SESSION['ADMIN']) {
				if (isset($_POST) AND !empty($_POST)) {
					$this->Commentaire->suppComment($_POST['id']);
					redirect(site_url(array('Administration', 'ListComment')));
				}else{echo "string";}
			}else {
				session_destroy();
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		}


	//fonction qui affiche le formulaire de suppression de commentaire
		public function AffFormSupCom(){
			if ($_SESSION['ADMIN']) {
				$data['id']=$_POST['id'];
				$this->load->view('ADMIN/index');
				$this->load->view('template_al/navigation');
				$this->load->view('ADMIN/formulaire_suppression_com',$data);
				$this->load->view('ADMIN/footer');
			}else {
				session_destroy();
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		}


	// fonction qui ajoute tous les utilisateurs
		public function addUsers(){
			if (isset($_SESSION['ADMIN'])) {
				if (isset($_POST)) {
					if (isset($_FILES['photo_profil']) and $_FILES['photo_profil']['error'] == 0) {
						// Testons si le fichier n'est pas trop gros
						if ($_FILES['photo_profil']['size'] <= 100000000) {
							// Testons si l'extension est autorisée
							$infosfichier = pathinfo($_FILES['photo_profil']['name']);
							$extension_upload = $infosfichier['extension'];

							$config = $_FILES['photo_profil']['name'] . date('d') . '-' . date('m') . '-' . date('Y') . 'a' . date('H') . '-' . date('i') . $_SESSION['ADMIN']['id'];
							$ma_variable = str_replace('.', '_', $config);
							$ma_variable = str_replace(' ', '_', $config);
							$config = $ma_variable . '.' . $extension_upload;
							move_uploaded_file($_FILES['photo_profil']['tmp_name'], 'assets/images/' . $config);
							$data['photo_profil'] = $config;
						} else {
							$data['message_save'] = "La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
							$data['message'] = 'non';
						}
					} else {
						$data['message_save'] = "L'image choisie  est endommagée  veuillez le remplacer svp !!";
						$data['message'] = 'non';
					}

					$data['nom'] = $_POST['nom'];
					$data['email'] = $_POST['email'];
					$data['niveau'] = $_POST['niveau'];

					$data['date'] = date('Y-m-d H:i:s');
					$this->Users->hydrate($data);
					$data['id_users']=$this->Users->addUser();
					// print_r($data['id_users']);
					if ($_POST['niveau']==1) {
						$data['pwd']=$_POST['pwd'];
						$data['id_users']=$data['id_users'];
						$this->Admin->hydrate($data);
						$this->Admin->addAdmin();
					}elseif($_POST['niveau']==3) {
						$data['pwd']=$_POST['pwd'];
						$data['id_users']=$data['id_users'];
						$this->Redacteur->hydrate($data);
						$this->Redacteur->addRed();
					}else{
						$data['pwd']=$_POST['pwd'];
						$data['id_users']=$data['id_users'];
						$this->Moderateur->hydrate($data);
						$this->Moderateur->addMod();
					}
					$_SESSION['message_save'] = "Administrateurs enregistré avec success !!";
					$_SESSION['success'] = 'ok';
					redirect(site_url(array('Administration', 'ListUsers'))); 
				}
			} else {
				session_destroy();
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		}

	// fonction qui permet d'ajouter un utilisateur
		public function AddUser(){

			if (isset($_SESSION['ADMIN'])) {
				$data['AllUser']=$this->Users->findAllUsersBd();
				$this->load->view('ADMIN/index');
				$this->load->view('template_al/navigation');
				$this->load->view('ADMIN/user_home',$data);
				$this->load->view('ADMIN/footer');
			} else {
				session_destroy();
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		}

	// fonction qui permet de modifier un admin
		public function ModAd(){
			if (isset($_SESSION['ADMIN'])) {
				$data['email']=$_POST['email'];
				$data['pwd']=$_POST['pwd'];
				$data['nom']=$_POST['nom'];
				$data['id_users']=$_SESSION['ADMIN']['id_users'];
				if (($_POST['nom']!=$_SESSION['ADMIN']['nom']) OR ($_POST['email']!=$_SESSION['ADMIN']['email']) OR ($_POST['pwd']!=$_SESSION['ADMIN']['pwd'])) {
					if (($_POST['nom']!=$_SESSION['ADMIN']['nom'])) {
						$this->Users->hydrate($data);
						$this->Users->modUser($_SESSION['ADMIN']['id_users']);
						$this->Admin->hydrate($data);
						$this->Admin->modAdmin();
					} elseif ($_POST['email']!=$_SESSION['ADMIN']['email']) {
						$this->Users->hydrate($data);
						$this->Users->modUser($_SESSION['ADMIN']['id_users']);
						$this->Admin->hydrate($data);
						$this->Admin->modAdmin();
					}elseif ($_POST['pwd']!=$_SESSION['ADMIN']['pwd']){
						$this->Users->hydrate($data);
						$this->Users->modUser($_SESSION['ADMIN']['id_users']);
						$this->Admin->hydrate($data);
						$this->Admin->modAdmin();
					}else{
					$this->Users->hydrate($data);
					$this->Users->modUser();
					$this->Admin->hydrate($data);
					$this->Admin->modAdmin();
					}
					redirect(site_url(array('Administration', 'index')));
				}else{
					$this->Users->hydrate($data);
					$this->Users->modUser();
					$this->Admin->hydrate($data);
					$this->Admin->modAdmin();
					redirect(site_url(array('Administration', 'index')));
					
					}
			}else {
				session_destroy();
				redirect(site_url(array('Administration', 'formulaireConnexion')));
				}	
		}

	//fonction qui affiche le profil d'un admin
		public function profile(){
			if (isset($_SESSION['ADMIN'])) {
				$this->load->view('ADMIN/index');
				$this->load->view('template_al/navigation');
				$this->load->view('ADMIN/profile');
				$this->load->view('ADMIN/footer');
			} else {

				redirect(site_url(array('Administration', 'index')));	
			}
		}

	// fonction qui affiche un user
		public function profileUser(){
			if (isset($_SESSION['ADMIN'])) {
				$data['id']=$_POST['id'];
				$data['nom']=$_POST['nom'];
				$data['niveau']=$_POST['niveau'];
				$data['email']=$_POST['email'];
				$data['photo_profil']=$_POST['photo_profil'];
				$this->load->view('ADMIN/index');
				$this->load->view('template_al/navigation');
				$this->load->view('ADMIN/formulaire_renseignement', $data);
				$this->load->view('ADMIN/footer');
			} else {

				redirect(site_url(array('Administration', 'index')));	
			}
		}

	//fonction qui deconnecte un admin
		public function deconnexion(){
			if (isset($_SESSION['ADMIN'])) {
				session_destroy();
			}
			redirect(site_url(array('Administration', 'index')));
		}
}
